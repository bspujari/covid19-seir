status:
	git status

pull:
	git pull -v origin master

add:
	git add * # A potential danger with 'git add *' is that it may add unwanted/intermediate files: Use ./.gitignore to bypass such files.

commit:
	git commit -a

ac: add commit

push:
	git push -v -u origin master

acp: add commit push

# one-time setup

user.name  := mihir.arjunwadkar
user.email := $(user.name)@gmail.com

git.server := http://gitlab.com/
git.repo   := bspujari/covid19-seir

config:
	git config --global user.email "$(user.email)"
	git config --global user.name "$(user.name)"
	git config --global core.autocrlf input
	git config --global core.safecrlf true
	git config --global core.editor "vim"

init:
	git init

addorigin:
	git remote add origin $(git.server)/$(git.repo)

init.all: config init addorigin 
