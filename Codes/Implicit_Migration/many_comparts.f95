Program many_comparts
    use rk4_integrate
    use, intrinsic :: iso_fortran_env
    implicit none
    integer*8 :: i, j, jj, u, v, n1, n2, n3, ix, num_vert, tot_time, max_deg, max_delay, tot_init_infected
    !begin/end_lcdn : the day on which the lockdown begins/ends. 
    integer*8, allocatable :: deg(:), delay(:, :)
    integer*8, allocatable :: init_cities(:), adj_list(:, :)
    real*8, allocatable :: D_array(:, :)
    real*8 :: dt, D, dist
    real*8, allocatable :: x(:, :) , age_groups_fracs(:), work_groups_fracs(:), pop(:)
    real*8, allocatable :: init_exposed_amount(:), init_sym_amount(:), init_asym_amount(:)
    character(120) :: read_line
    character, dimension(20) :: read_array*120 !(18) ! num of lines in parameter.dat file
    character (len=90) :: filename
    logical :: file_exists
    read_array=''
    ! Read the parameters
    open(unit = 1, file = 'parameters.dat')

  ! i=1
  ! read_loop: do
  !     read (1, '(A)', iostat=j )  read_line 
  !     if ( j== iostat_end)  exit read_loop 
  !     if ( j/= 0 ) then
  !         print *, "ERR"
  !         exit read_loop
  !     end if
  !     if ( index (read_line, "#") /= 0 )  cycle read_loop
  !     if ( index (read_line, "!") /= 0 )  cycle read_loop
  !     read (read_line, *)read_array(i)
  !     i=i+1
  ! end do read_loop
  ! close(1)
  ! print *, read_array
   ! Every alternate line has comment starting from 1st one
    read(1,*) read_line 
    read(1,*) num_vert  
    read(1,*) read_line 
    read(1,*) dt
    read(1,*) read_line 
    read(1,*) n1
    read(1,*) read_line 
    read(1,*) n2
    read(1,*) read_line 
    read(1,*) n3
    read(1,*) read_line 
    read(1,*) D
    read(1,*) read_line 
    read(1,*) tot_time
    read(1,*) read_line 
    read(1,*) tot_init_infected
    read(1,*) read_line 
    read(1,*) q_tau
    read(1,*) read_line 
    read(1,*) num_intv
    allocate (lcdn_type(num_intv), begin_lcdn(num_intv), end_lcdn(num_intv),work_period(num_intv), &
     &       lcdn_period(num_intv),stag_period(num_intv), stag_workdays(num_intv))
    read(1,*) read_line 
    read(1,*) lcdn_type
    read(1,*) read_line 
    read(1,*) begin_lcdn
    read(1,*) read_line 
    read(1,*) end_lcdn
    read(1,*) read_line 
    read(1,*) work_period
    read(1,*) read_line 
    read(1,*) lcdn_period
    read(1,*) read_line 
    read(1,*) stag_workdays
    read(1,*) read_line 
    read(1,*) stag_period 
    read(1,*) read_line 
    read(1,*) lcdn_val

    allocate(D_array(num_vert, num_vert))
    D_array = D

!   num_vert=int(read_array(1))
!   dt=read_array(2)
!   n1=int(read_array(3))
!   n2=int(read_array(4))
!   n3=int(read_array(5))
!   D=read_array(6)
!   tot_time=int(read_array(7))
!   tot_init_infected=int(read_array(8))
!   q_tau=read_array(9)
!   num_intv = int(read_array(10)) ! number of interventions
!   if (num_intv .gt. 1) then
!           print *, "Error! number of intervemtions cannot be more than 1"
!           stop 
!   endif 

!   !lcdn_type, begin_lcdn, end_lcdn, work_period, lcdn_period, stag_period, stag_workdays
!   lcdn_type =int(read_array(11)) 
!   begin_lcdn=int(read_array(12)) 
!   end_lcdn   =int(read_array(13))  
!   work_period=int(read_array(14))
!   lcdn_period=int(read_array(15))
!   stag_workdays=int(read_array(16))
!   stag_period=int(read_array(17))
!   lcdn_val=read_array(18)

    allocate(deg(num_vert), delay(num_vert, num_vert), pop(num_vert), region_type(num_vert))

    open(unit = 10, file = 'model_parameters.dat') ! parameters specific to virus
    read(10, *) se, eip, eia, lambda_p, iar, lambda_s, imr, rho
    close(10)

    region_type = 0
    INQUIRE (file = 'pop_region_type.dat', EXIST=file_exists)
    if ( file_exists ) then 
        print*, "Reading the population type from file. This should be done for district-wise run only."  
        open(unit = 21, file = 'pop_region_type.dat')
        read(21, *)region_type
        close(21)
    endif
    ! Rates corresponding to E -> (Ia & Ip) can be written as : 
    !           E -> ei*alpha -> Ip
    !           E -> ei*(1-alpha) -> Ia
    ! Therefore in input file eip = ei*alpha and eia = ei*(1-alpha)  

    ! Similarly if lambda is the rate of transtion from Ip to (Im & Is) then
    !           Ip -> lambda*mu -> Im
    !           Ip -> lambda*(1-mu) -> Is 
    ! Threfore in input file ipis = lambda*mu and ipim=lambda*(1-mu) 

    ! Read the populations
    open(unit = 1, file = 'populations.dat')
    DO i = 1, num_vert
        read(1, *)u, v
        pop(i) = float(v) / 25000. 
    ENDDO
    close(1)

    ! Read the edge list and find the maximum degree
    deg = 0
    open(unit = 1, file = 'edges.txt')
    DO i = 1, 10000000
        read(1, *, end = 10) u, v, dist
        deg(u) = deg(u) + 1
        deg(v) = deg(v) + 1
    ENDDO
10  continue
    close(1)

    ! Construct the adj list and delay matrix
    max_deg = maxval(deg)
    allocate(adj_list(num_vert, max_deg))
    deg = 0
    adj_list = 0
    delay = 0
    open(unit = 1, file = 'edges.txt')
    DO i = 1, 10000000
        read(1, *, end = 20) u, v, dist
        deg(u) = deg(u) + 1
        deg(v) = deg(v) + 1
        adj_list(u, deg(u)) = v
        adj_list(v, deg(v)) = u
        delay(u, v) = int(dist / (50.0 * 24.0 * dt), 8) ! Assuming the speed to be 50km/h
    ENDDO
20  continue
    close(1)
    max_delay = maxval(delay)

    ! Initialize the compartments
    ! Initially all are susceptibles
    allocate(x(n1*n2*n3 * num_vert, tot_time + max_delay))
    allocate(age_groups_fracs(n2), work_groups_fracs(n3))
    !allocate(age_rate_factor(n2))
    !allocate(age_death_factor(n2))
    !age_death_factor=1.
    !age_rate_factor=1.
    if (n2 .eq. 6) then
        !tn=71441228  Tamilnadu
        !.66277700321724592975 : 0-39
        !.13685745995295601581 : 40-49
        !.09524755089596164276 : 50-59
        !.06510215641869985773 : 60-69
        !.02962952988434073389 : 70-79
        !.01038629963079582002 : 80+
        !age distributeion https://en.wikipedia.org/wiki/Demographics_of_India
        age_groups_fracs=(/.662777003217, .1368574599, .095247550895, .06510215642, .02962952988, .01038629963 /)
        !age_rate_factor= (/1.0, 4.0833333, 8.5, 13.833333, 20.25, 22.75 /)
        !age_death_factor= (/ 1. , .998899, .994298, .978293488,.949284785,.90727218 /)
    else
        age_groups_fracs = 1./float(n2)
        !age_death_factor=1.
        !age_rate_factor=1.
    endif
    work_groups_fracs = 1./float(n3)
    x = 0    
    DO i = 1, n1*n2*n3 * num_vert, n1*n2*n3
        ix = (i-1)/(n1*n2*n3) + 1
        Do j = 1, n2 ! Loop over age groups
            ! The population of each node is divided into different 
            ! susceptible groups
            do jj = 1, n3
                x(i-1+(j-1)*n3+jj, :) = age_groups_fracs(j) * work_groups_fracs(jj) * pop(ix)
            enddo
        Enddo
    ENDDO

    ! Expose a few cities initially
    if (tot_init_infected .eq.  0) then
        print*, "WARNING. All cities infected. init_condition.dat file ignored."
        tot_init_infected=num_vert
        allocate(init_cities(tot_init_infected), init_exposed_amount(tot_init_infected), &
                & init_sym_amount(tot_init_infected), init_asym_amount(tot_init_infected))
        do i = 1, num_vert
                init_cities(i)=i
                init_exposed_amount(i)=1.e-4
                init_asym_amount=1.e-5
                init_sym_amount(i)=1.e-6
        enddo
    else
        allocate(init_cities(tot_init_infected), init_exposed_amount(tot_init_infected), &
                & init_sym_amount(tot_init_infected), init_asym_amount(tot_init_infected))
        open(unit = 22, file = 'init_conditions.dat')
        DO i = 1, tot_init_infected
                read(22, *)init_cities(i), init_exposed_amount(i), init_sym_amount(i), init_asym_amount(i)
        ENDDO
    endif 
    
    DO i = 1, size(init_cities)
        ix = init_cities(i)
        ! Increase exposed
        Do j = 1, n2
            do jj = 1, n3
                x((ix-1)*n1*n2*n3 + 1*n2*n3 + (j-1)*n3 + jj, :) & 
                    & = init_exposed_amount(i)  * x((ix-1)*n1*n2*n3 + (j-1)*n3 + jj, :)
                x((ix-1)*n1*n2*n3 + 2*n2*n3 + (j-1)*n3 + jj, :) &
                    & = init_sym_amount(i)* x((ix-1)*n1*n2*n3 + (j-1)*n3 + jj, :)
                x((ix-1)*n1*n2*n3 + 3*n2*n3 + (j-1)*n3 + jj, :) &
                    & = init_asym_amount(i)* x((ix-1)*n1*n2*n3 + (j-1)*n3 + jj, :)
            enddo
        Enddo
        ! Reduce susceptibles
        Do j = 1, n2
            x((ix-1)*n1*n2*n3 + jj, :) = (1.-init_exposed_amount(i)&
               &-init_sym_amount(i)-init_asym_amount(i)) * x((ix-1)*n1*n2*n3 + jj, :)
        Enddo
    ENDDO
    
    ! Open output files
    open(unit = 101, file = 'Output_data/susceptibles.dat')
    open(unit = 102, file = 'Output_data/exposed.dat')
    open(unit = 103, file = 'Output_data/presympt.dat')
    open(unit = 104, file = 'Output_data/asympt.dat')
    open(unit = 105, file = 'Output_data/sympt_severe.dat')
    open(unit = 106, file = 'Output_data/sympt_minor.dat')
    open(unit = 107, file = 'Output_data/hospitalized.dat')
    open(unit = 108, file = 'Output_data/recovered.dat')
    open(unit = 109, file = 'Output_data/died.dat')
    open(unit = 110, file = 'Output_data/peaks.dat')
    if (n2 .eq. 6) then
        fileLoop: do j = 1,n2
             write (filename, '( "Output_data/hospitalized_", I1, ".dat" )' ) j
             OPEN(unit=1000+j*10,file=filename)
             write (filename, '( "Output_data/total_", I1, ".dat" )' ) j
             OPEN(unit=1100+j*10,file=filename)
        end do fileLoop
    endif

    !if (n3 .eq. 3) then
        work_fileLoop: do jj = 1, n3
            write (filename, '( "Output_data/hospitalized_work_", I1, ".dat")') jj
            open(unit = 2000+jj*10, file = filename)
            write (filename, '( "Output_data/total_infected_work_", I1, ".dat")') jj
            open(unit = 2100+jj*10, file = filename)
        end do work_fileLoop
    !endif
    
    call RK4(x, n1, n2, n3, num_vert, tot_time, max_deg, max_delay, pop, deg, delay, D_array, dt, adj_list)    

End program many_comparts

