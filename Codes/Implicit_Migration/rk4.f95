Module rk4_integrate
    use equations
    implicit none
    contains
    Subroutine RK4(x, n1, n2, n3, num_vert, tot_time, max_deg, max_delay, pop, deg, delay, D_array, dt, adj_list)    
        implicit none
        integer*8 :: i, j, l, jj, t, n1, n2, n3, ptr, num_vert, tot_time, max_deg, max_delay, period, nbr
        integer*8 :: deg(num_vert), delay(num_vert, num_vert), adj_list(num_vert, max_deg)
        real*8 :: dt, sum_h(num_vert, n3), sum_ti(num_vert, n3), D_orig, se_orig, peak_time(n1*n2*n3*num_vert)
        real*8 :: pop(num_vert), nbrs_pop_sum(num_vert), x(n1*n2*n3*num_vert, tot_time + max_delay)
        real*8 :: xprime(n1*n2*n3*num_vert), k(n1*n2*n3*num_vert, 4), peak(n1*n2*n3*num_vert), D_array(num_vert, num_vert)
    
        ! Integration starts here
        se_orig=se
        peak = x(:, max_delay+1)
        peak_time = 1
        period = 14

        ! Calculate the sum of the populations of neighbours
        nbrs_pop_sum = 0
        DO i = 1, num_vert
            Do j = 1, deg(i)
                nbr = adj_list(i, j)
                nbrs_pop_sum(i) = nbrs_pop_sum(i) + pop(nbr)
            Enddo
        ENDDO

        ! Loop over time
        DO t = max_delay + 1, tot_time + max_delay - 1

            !write(*,'(I10,A)',ADVANCE='NO') t, CHAR(13)
            call derivative(n1, n2, n3, num_vert, t, dt, tot_time, max_deg, max_delay, deg, x, xprime, delay, pop, &
                & D_array, nbrs_pop_sum, adj_list)
            k(:, 1) = xprime(:)
    
            x(:, t+1) = x(:, t) + 0.5 * k(:, 1) * dt
            call derivative(n1, n2, n3, num_vert, t, dt, tot_time, max_deg, max_delay, deg, x, xprime, delay, pop, &
                & D_array, nbrs_pop_sum, adj_list)
            k(:, 2) = xprime(:) 
    
            x(:, t+1) = x(:, t) + 0.5 * k(:, 2) * dt
            call derivative(n1, n2, n3, num_vert, t, dt, tot_time, max_deg, max_delay, deg, x, xprime, delay, pop, &
                & D_array, nbrs_pop_sum, adj_list)
            k(:, 3) = xprime(:)
    
            x(:, t+1) = x(:, t) + k(:, 3) * dt
            call derivative(n1, n2, n3, num_vert, t, dt, tot_time, max_deg, max_delay, deg, x, xprime, delay, pop, &
                & D_array, nbrs_pop_sum, adj_list)
            k(:, 4) = xprime(:)
    
            x(:, t+1) = x(:, t) + (1/6.) * (k(:, 1) + 2. * k(:, 2) + 2. * k(:, 3) + k(:, 4)) * dt

            ! Detect the peak value and the time
            !Do i = 1, n1*n2*num_vert
            !    if (x(i, t+1) > peak(i))then
            !        peak(i) = x(i, t+1)
            !        peak_time(i) = (t - max_delay)*dt
            !    endif
            !Enddo


            ! If any compartment value corresponds to less than 1 person, make it zero
           Do i = 1, num_vert
                ptr = (i-1)*n1*n2*n3
               !where (x(ptr + 1: ptr + n2*n3, t+1) <= 1./25000.) x(ptr + 1: ptr + n2*n3, t+1) = 0.d0
               where (x(ptr + n2*n3 + 1 : ptr + 2*n2*n3, t+1) <= 1./25000.) x(ptr + n2*n3 + 1 : ptr + 2*n2*n3, t+1) = 0.d0

               if (all(x(ptr + 2*n2*n3 + 1: ptr + 6*n2*n3, t+1) <= 1./25000))then
                  x(ptr+2*n2*n3+1:ptr+6*n2*n3, t+1) = 0.d0
               endif

           Enddo
    
            ! For now, writing only susceptibles and exposed
            if (mod(t, 1) == 0)then

                write(101, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2*n3 + 1        : (i-1)*n1*n2*n3 + n2*n3, t)),   i = 1, num_vert)
                write(102, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2*n3 + n2*n3 + 1 : (i-1)*n1*n2*n3 + 2*n2*n3, t)), i = 1, num_vert)
                write(103, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2*n3 + 2*n2*n3 + 1 : (i-1)*n1*n2*n3 + 3*n2*n3, t)),  i = 1, num_vert)
                write(104, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2*n3 + 3*n2*n3 + 1 : (i-1)*n1*n2*n3 + 4*n2*n3, t)), i = 1, num_vert)
                write(105, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2*n3 + 4*n2*n3 + 1 : (i-1)*n1*n2*n3 + 5*n2*n3, t)), i = 1, num_vert)
                write(106, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2*n3 + 5*n2*n3 + 1 : (i-1)*n1*n2*n3 + 6*n2*n3, t)), i = 1, num_vert)
                write(107, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2*n3 + 6*n2*n3 + 1 : (i-1)*n1*n2*n3 + 7*n2*n3, t)), i = 1, num_vert)
                write(108, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2*n3 + 7*n2*n3 + 1 : (i-1)*n1*n2*n3 + 8*n2*n3, t)), i = 1, num_vert)
                write(109, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2*n3 + 8*n2*n3 + 1 : (i-1)*n1*n2*n3 + 9*n2*n3, t)), i = 1, num_vert)

                ! Write work class files
                DO i = 1, num_vert
                    Do jj = 1, n3
                        ! Calculate hospitalized
                        sum_h(i, jj) = 0
                        do j = 1, n2
                            sum_h(i, jj) = sum_h(i, jj) + x((i-1)*n1*n2*n3 + 6*n2*n3 + (j-1)*n3 + jj, t)
                        enddo
                        ! Calculate total infected
                        sum_ti(i, jj) = 0
                        do l = 2, 5
                            do j = 1, n2
                                sum_ti(i, jj) = sum_ti(i, jj) + x((i-1)*n1*n2*n3 + l*n2*n3 + (j-1)*n3 + jj, t)
                            enddo
                        enddo
                    Enddo
                ENDDO
                DO jj = 1, n3
                    write(2000+jj*10, *)(t-max_delay)*dt, (sum_h(i, jj), i = 1, num_vert)
                    write(2100+jj*10, *)(t-max_delay)*dt, (sum_ti(i, jj), i = 1, num_vert)
                ENDDO
            !    if (n2 == 6 ) then ! write age separated files
            !      !do l = 0,n1-1 
            !        do j=1,n2
            !            write(1000+(10*j),*) (t-max_delay)*dt,  (x((i-1)*n1*n2 + 6*n2+ j,t), i=1,num_vert) ! hospitalized
            !            write(1100+(10*j),*) (t-max_delay)*dt,  (x((i-1)*n1*n2 + 2*n2+ j,t) &
            !                    & +x((i-1)*n1*n2 + 3*n2+ j,t)+x((i-1)*n1*n2 + 4*n2+ j,t) &
            !                    & +x((i-1)*n1*n2 + 5*n2+ j,t), i=1,num_vert) ! total infected 
            !         enddo
            !      !enddo
            !    endif
            endif

        ENDDO
        print*, " "
        !Do i = 1, n1*n2*num_vert
        !    write(110, *)peak(i), peak_time(i)
        !Enddo
    End Subroutine RK4

End module rk4_integrate
