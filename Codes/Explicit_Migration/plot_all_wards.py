import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import cm
from descartes import PolygonPatch
import json # or 
import geojson
import geopandas as gpd
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

def rename_cities(df,cities):
    return df.rename(columns = {i+1 : cities[i] for i in range(len(cities))}, inplace = True)

def add_figure(f,x,df,legend,color):
    f.line(x,df,line_width=3,legend_label=legend,color=color)
    return f

def scale_values(ex):# because the population is in the units of 25000
    ex.iloc[:,1:]=ex.iloc[:,1:].multiply(25000)
    return ex

def printmd(string):
    display(Markdown(string))
    
def namestr(obj, namespace):
    return [name for name in namespace if namespace[name] is obj]
 
def read_n_scale_wg():
    R0=2.4
    hos_df_1=pd.read_csv("Output_data/hospitalized_work_1_"+str(R0)+".dat",sep="\s+",header=None)
    hos_df_2=pd.read_csv("Output_data/hospitalized_work_2_"+str(R0)+".dat",sep="\s+",header=None)
    hos_df_3=pd.read_csv("Output_data/hospitalized_work_3_"+str(R0)+".dat",sep="\s+",header=None)
    tot_df_1=pd.read_csv("Output_data/total_infected_work_1_"+str(R0)+".dat",sep="\s+",header=None)
    tot_df_2=pd.read_csv("Output_data/total_infected_work_2_"+str(R0)+".dat",sep="\s+",header=None)
    tot_df_3=pd.read_csv("Output_data/total_infected_work_3_"+str(R0)+".dat",sep="\s+",header=None)
    
    cities = pd.read_csv("vert_index_with_names.dat", sep = ",")
    cities = list(cities["name"])
    cities = [name.title() for name in cities]
    for df in [hos_df_1,hos_df_2,hos_df_3, tot_df_1, tot_df_2, tot_df_3]:
        rename_cities(df,cities)
        df=scale_values(df)
    
    return hos_df_1,hos_df_2,hos_df_3, tot_df_1, tot_df_2, tot_df_3



hos_df_1,hos_df_2,hos_df_3, tot_df_1, tot_df_2, tot_df_3=read_n_scale_wg()




plt.style.use('seaborn')

Tot = 15
Cols = 3
Rows = Tot // Cols
Rows += Tot % Cols
cities = pd.read_csv("vert_index_with_names.dat", sep = ",")
cities = list(cities["name"])
cities = [name.title() for name in cities]

Position = range(1,Tot + 1)
fig = plt.figure(figsize=(12,17))
#fig.suptitle("onemore",fontsize=13,color='r')
#plt.text(0,.95,"Periodic lockdown with 5 days working + 9 days of lockdown.\nBest-case and Worst-Case scenarios are based on how well the crowd is controlled.",color='tab:blue',fontsize=15)
fig.suptitle("Projected Critical Hospitalization in Pune With Testing",fontsize=18,color='r')
for k in range(Tot):
    city=cities[k]
    ax  = fig.add_subplot(Rows,Cols,Position[k])
    x=hos_df_1[0]
    plt.axvline(x=39,ls="-",color="grey",alpha=.5)
    plt.text(.15,.9,"Lockdown",transform=ax.transAxes,)
    plt.text(.4,.9,"Periodic Lockdown",transform=ax.transAxes,)
    ax.plot(x,hos_df_1[city],'g',linewidth=1)
    ax.plot(x,hos_df_2[city],'r',linewidth=1)
    ax.plot(x,hos_df_3[city],'b',linewidth=1)
    #ax.fill_between(x, hos_df[city],hos_df2[city], alpha=.2)
    #ax.plot(x,hos_df_3[city],linewidth=3)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    plt.ylabel("Number",fontsize=10)
    plt.xlabel("Days",fontsize=10)
    ax.annotate('8 April', xy=(.0, .0),  xycoords='data',
            xytext=(.05, .3), textcoords='axes fraction',
            arrowprops=dict(facecolor='black', shrink=0.05),
            horizontalalignment='center', verticalalignment='top',
            )
    plt.xlim(right=90)
    #plt.text(.6,.35,"Best case",transform=ax.transAxes,color='g')
    #plt.text(.6,.75,"Worst case",transform=ax.transAxes,color='r')
    plt.title(city,fontsize=12,color='maroon')
fig.text(0.05,.93,"Periodic lockdown with 5 days working + 9 days of lockdown, implemented after the current lockdown.\nBest-case and Worst-Case scenarios are based on how well the crowd is controlled.",color='tab:blue',fontsize=15)
fig.tight_layout(rect=[0, 0., 1, .92])
plt.show()

fig = plt.figure(figsize=(12,17))
fig.suptitle("Projected Critical Hospitalization in Pune With Testing",fontsize=18,color='r')
for k in range(Tot):
    city=cities[k]
    ax  = fig.add_subplot(Rows,Cols,Position[k])
    x=hos_df_1[0]
    plt.axvline(x=39,ls="-",color="grey",alpha=.5)
    plt.text(.15,.9,"Lockdown",transform=ax.transAxes,)
    plt.text(.4,.9,"Periodic Lockdown",transform=ax.transAxes,)
    ax.plot(x,tot_df_1[city],'g',linewidth=1)
    ax.plot(x,tot_df_2[city],'r',linewidth=1)
    ax.plot(x,tot_df_3[city],'b',linewidth=1)
    #ax.fill_between(x, hos_df[city],hos_df2[city], alpha=.2)
    #ax.plot(x,hos_df_3[city],linewidth=3)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    plt.ylabel("Number",fontsize=10)
    plt.xlabel("Days",fontsize=10)
    ax.annotate('8 April', xy=(.0, .0),  xycoords='data',
            xytext=(.05, .3), textcoords='axes fraction',
            arrowprops=dict(facecolor='black', shrink=0.05),
            horizontalalignment='center', verticalalignment='top',
            )
    plt.xlim(right=90)
    #plt.text(.6,.35,"Best case",transform=ax.transAxes,color='g')
    #plt.text(.6,.75,"Worst case",transform=ax.transAxes,color='r')
    plt.title(city,fontsize=12,color='maroon')
fig.text(0.05,.93,"Periodic lockdown with 5 days working + 9 days of lockdown, implemented after the current lockdown.\nBest-case and Worst-Case scenarios are based on how well the crowd is controlled.",color='tab:blue',fontsize=15)
fig.tight_layout(rect=[0, 0., 1, .92])
plt.show()
