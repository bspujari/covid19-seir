import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.style.use('seaborn')

#sus_df=pd.read_csv("Output_data/susceptibles_9d.dat",sep="\s+",header=None)
expos_df=pd.read_csv("Output_data/exposed_9d.dat",sep="\s+",header=None)
asymp_df=pd.read_csv("Output_data/asympt_9d.dat",sep="\s+",header=None)
presymp_df=pd.read_csv("Output_data/presympt_9d.dat",sep="\s+",header=None)
symp_min_df=pd.read_csv("Output_data/sympt_minor_9d.dat",sep="\s+",header=None)
symp_sev_df=pd.read_csv("Output_data/sympt_severe_9d.dat",sep="\s+",header=None)
hos_df=pd.read_csv("Output_data/hospitalized_9d.dat",sep="\s+",header=None)
rec_df=pd.read_csv("Output_data/recovered_9d.dat",sep="\s+",header=None)
death_df=pd.read_csv("Output_data/died_9d.dat",sep="\s+",header=None)
#print(symp_min_df.max())
fig = plt.figure(figsize=(10,8))
ax  = fig.add_subplot(111)
x=expos_df[0]
ind = 1
ax.plot(x,25000*expos_df[ind],linewidth=2, label="Exposed")
ax.plot(x,25000*asymp_df[ind],linewidth=2,label="Asymptomatic")
ax.plot(x,25000*presymp_df[ind],linewidth=2,label="Pre-symptomatic")
ax.plot(x,25000*symp_min_df[ind],linewidth=2,label="Mild Symptomatic")
ax.plot(x,25000*symp_sev_df[ind],linewidth=2,label="Severe Symptomatic")
ax.plot(x,25000*hos_df[ind],linewidth=2,label="Hospitalized")
#ax.plot(x,25000*rec_df[ind],linewidth=2,label="Recovered")
ax.plot(x,25000*death_df[ind],linewidth=2,label="Deaths")
ax.plot(x,25000*(asymp_df[ind] + presymp_df[ind] + symp_min_df[ind] + symp_sev_df[ind]),linewidth=2,label="Total infected")
plt.ylabel("x*25000",fontsize=15)
plt.legend()
#plt.savefig("picture.pdf")
#plt.yscale('log')
#plt.grid()
plt.xlabel('Days', fontsize = 15)
plt.ylabel('Total number', fontsize = 15)
plt.tight_layout()
#plt.savefig('tot_infected_Rw2.4_RL0.95.jpeg')
plt.show()

