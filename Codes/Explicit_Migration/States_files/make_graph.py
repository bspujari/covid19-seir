"""
    Construct and save a graph from the edge list
"""
import pandas as pd
import graph_tool.all as gt

df = pd.read_csv('state_edges_with_names.txt', header = None)
g = gt.Graph(directed = False)
name = g.add_edge_list(df.values, hashed = True)
print(g)
g.vp['name'] = name
g.save('indian_states_graph.gt')


