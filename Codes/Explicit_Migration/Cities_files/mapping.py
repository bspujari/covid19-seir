"""
Process rail-graph to produce edge list and vertex population files
"""
import scipy
import pandas as pd
import matplotlib.pyplot as plt
import graph_tool.all as gt

g = gt.load_graph("rail_graph.gt")
deg = g.get_out_degrees(range(g.num_vertices()))
name = g.vp["name"]
dist = g.ep["dist"]
print(g)

df1 = pd.DataFrame.from_dict({int(v) : name[v] for v in g.vertices()}, orient = "index")
df1["vert_index"] = df1.index + 1
df1.rename(columns = {0 : "name"}, inplace = True)
df1["name"] = df1["name"].apply(lambda s : s.title())

df2 = pd.read_csv("Train_station_population.csv")
df2.drop_duplicates(inplace = True)
df2["name"] = df2["name"].apply(lambda s : s.title())

df = df1.merge(df2, on = "name", how = "left")
df[["vert_index", "population"]].to_csv("populations_cities.dat", index = False, sep = " ")

print(df.head(36))

df[["vert_index", "name", "population"]].to_csv("vert_index_with_names.dat", index = False)

# Construct the edge list
with open('edges.txt', 'w') as f:
    for e in g.edges():
        u, v = int(e.source()), int(e.target()) 
        # We add 1 because fortran indices start from 1
        f.write(f"{u+1} {v+1} {dist[e]}\n")

