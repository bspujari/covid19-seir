import numpy as np
import pandas as pd

#df = pd.read_excel('District_level_data.xlsx', skip_blank_lines = True, skiprows = [1, 2, 3])
df = pd.read_excel('districts.xlsx', skip_blank_lines = True, skiprows = list(range(7)), header = None)

# Rename columns 
df.rename(columns = {0 : 'state_code', 1 : 'district_code', 
2 : 'sub_district_code', 3 : 'region_type', 4 : 'name', 5 : 'pop_type', 
6 : 'inhabited_villages', 7 : 'uninhabited_villages', 8 : 'tot_towns', 
9 : 'households', 10 : 'tot_pop', 11 : 'males', 12 : 'females', 
13 : 'area_sq_km', 14 : 'pop_per_sq_km' }, inplace = True)

# Just keep useful columns
df = df[['state_code', 'region_type', 'district_code', 'name', 'pop_type', 'tot_pop']]
# Now drop NaN
df.dropna(inplace = True)

# Convert total population to int
df['state_code'] = df['state_code'].astype('int')
df['tot_pop'] = df['tot_pop'].astype('int')

# Keep only rows corresponding to the districts
df = df[df['region_type'] == 'DISTRICT']

# Construct rural and urban frames
df_rural = df[df['pop_type'] == 'Rural']
df_urban = df[df['pop_type'] == 'Urban']
df_total = df[df['pop_type'] == 'Total']

print("length", len(df), len(df_rural), len(df_urban), len(df_total))

# Merge the rural and urban frames so that the two populations appear as columns
df = df_rural.merge(df_urban, on = 'district_code', how = 'inner')
df.rename(columns = {'state_code_x' : 'state_code', 'name_x' : 'name', 'tot_pop_x' : 'pop_rural', 
    'tot_pop_y' : 'pop_urban'}, inplace = True)
# Only keep the required columns
df = df[['state_code', 'district_code', 'name', 'pop_rural', 'pop_urban']]
df['name'] = df['name'].apply(lambda s : s.strip())

# Classify each district as rural or urban
df['pop_tot'] = df['pop_rural'] + df['pop_urban']
df['region_type'] = df.apply(lambda row : 'rural' 
    if row['pop_rural'] > row['pop_urban'] else 'urban', axis = 1)

print(df.head(20))
print(len(df))

df.to_csv('districts.csv', index = False)

