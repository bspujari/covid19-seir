import pandas as pd

df = pd.read_csv('../districts.csv')
# Get Maharashtra districts
df = df[df['state_code'] == 27]
print(df)
print(len(df))

df2 = pd.read_csv('vert_names_with_index.dat', header = None, names = ['vert_index', 'name'])
print(df2)

df = df.merge(df2, on = 'name')
df.sort_values(by = 'vert_index', inplace = True)

print(df)

df[['vert_index', 'pop_tot']].to_csv('populations.dat', sep = ' ', index = False)
