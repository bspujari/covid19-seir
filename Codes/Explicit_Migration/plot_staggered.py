import pandas as pd
import matplotlib.pyplot as plt
plt.style.use('seaborn')

vert_names = pd.read_csv('vert_index_with_names.dat')

ind = 8 # vertex index to plot the time series
vert_name = list(vert_names[vert_names['id'] == ind]['name'])[0]
print(vert_name)
D = 0.0 # coupling strength
n3 = 3 # number of working classes
test = "_no_test"
#test = "_test"

for RL in [0.95]:
    for RW in [2.4]:
        for compart in ['infect', 'hosp']:
            # Plot total in the compartment for each working class
            dfs = [pd.read_csv(f'Output_data/' + compart + 
                f'_work{i}_Rw{RW}_RL{RL}_D{D}{test}.dat', sep = '\s+', 
                header = None) for i in range(1, n3+1)]
            for i, df in enumerate(dfs):
                plt.plot(df[0], 25000*df[ind], label = f'Working class {i+1}')
            # Plot the sum of the compartment across working classes 
            work_sum = pd.concat([df[ind] for df in dfs], axis=1).sum(axis = 1)
            plt.plot(dfs[0][0], 25000*work_sum, label = 'Total')              
            plt.legend(prop = {'size' : 15})
            
            plt.xlabel('Days', fontsize = 15)
            plt.ylabel('The total number', fontsize = 15)
            plt.title(fr'Total {compart} in {vert_name} $R_W = ${RW}, '
                f'$R_L = ${RL}, $D = ${D}', fontsize = 15)
            plt.savefig(f'Figures/{compart}_Rw{RW}_RL{RL}_D{D}_{vert_name}'
                f'{test}.jpeg')
            plt.close()
    
            # Plot total of the compartment in the network
            n = len(dfs[0].columns)
            cols = list(range(1, n))
            df = pd.concat([df[cols].sum(axis = 1) for df in dfs], axis = 1)
            plt.plot(dfs[0][0], 25000*df.sum(axis = 1))
            plt.xlabel('Days', fontsize = 15)
            plt.ylabel('The total number', fontsize = 15)
            plt.title(fr'Total {compart} in the network '
                f'$R_W = ${RW}, $R_L = ${RL}, $D = ${D}', fontsize = 15)
            plt.savefig(f'Figures/{compart}_Rw{RW}_RL{RL}_D{D}{test}.jpeg', 
                sep = '\s+', header = None)
            plt.close()    

