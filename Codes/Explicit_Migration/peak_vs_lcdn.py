import os
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Compile the fortran codes
os.system('gfortran -O3 model_eqns.f95 rk4.f95 many_comparts.f95')

num_vert = 32
dt = 0.1
n1 = 9
n2 = 1
D = 0.0
tot_time = 5000
tot_init_infected = 3
begin_lcdn = 30
vert_index = 19

for lcdn_val in np.arange(0.01, 0.28, 0.04):
    lcdn_range = range(begin_lcdn + 1, 300, 10)
    
    peak_vals = []
    for end_lcdn in lcdn_range:
        sys.stdout.write(f'lcdn_val = {lcdn_val}, lcdn end = {end_lcdn}')
        sys.stdout.flush()
        with open('parameters.dat', 'w') as f:
            f.write(f"{num_vert}\n")
            f.write(f"{dt}\n")
            f.write(f"{n1}\n")
            f.write(f"{n2}\n")
            f.write(f"{D}\n")
            f.write(f"{tot_time}\n")
            f.write(f"{tot_init_infected}\n")
            f.write(f"{begin_lcdn}\n")
            f.write(f"{end_lcdn}\n")
            f.write(f"{lcdn_val}\n")
        # Run the fortran code
        os.system('./a.out')
    
        # Save the peak
        df = pd.read_csv('Output_data/peaks_9d.dat', sep = '\s+', header = None)
        # Get peak for hospitalized of vert_index
        peak_vals.append(df.iloc[(vert_index-1)*n1*n2 + 6][0] * 25000)
    
    plt.plot(lcdn_range, peak_vals, 'o-', label = f"beta = {round(0.5 * lcdn_val, 3)}")
    plt.legend()

plt.ylabel('Total hospitalized', fontsize = 15)
plt.xlabel('Lockdown end day', fontsize = 15)
plt.grid()
plt.tight_layout()
plt.savefig('Figures/hospitalized_vs_lcdn.pdf')

