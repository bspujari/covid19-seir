Program SEIR
!-------------------------------------------------------------------------
! Multi-population SEIR model with explicit migration. 
! Based on : https://doi.org/10.1101/2020.03.13.20035386 
! A program by Snehal Shekatkar & Bhalchandra Pujari.
! Please do not circulate without permission.
! In case of bugs/queries please contact:
!       snehal.shekatkar@cms.unipune.ac.in
!       bspujari@cms.unipune.ac.in
! Version 1.0 01 April 2020. 
!..........................................................................
! Compilation instructions:
!       gfortran SEIR.f95 -o SEIR.exe   
! Optionally, for optimized speedup
!       gfortran -ffast-math -O3 -Ofast  SEIR.f95 -o SEIR.exe   
!..........................................................................
! File "edges.txt"  and "populations.dat"  
! The program needs networks of meta-populations. It is read from    
! `edges.txt` file. The format of the files is as follows:
!               i1 j1 dist   
!               i2 j2 dist
!               .  .  .
!               .  .  .
!  where i,j are the indices of nodes with geographical distance as third   
!  column. The populations of node are read from `populations.dat`. The
!  format of the same is:
!               i1   pop1
!               i2   pop2
!               .    .
!               .    .
!..........................................................................
! File "balance_params.dat"
! The migration is controlled via eta and D. eta is obtained separately 
! via a iterative self consistent process. Please see the original preprint
! for the details of formalism. The format is just a column of eta values
! for all meta-population: 
!               eta(1)
!               eta(2)
!               .
!               .
!..........................................................................
! File "parameters.dat"     
! Read the parameters of simulation
!..........................................................................
! Outputs are stored on "Output_data" folder. The subscript associated with
! the filenames indicate the age group.
!-------------------------------------------------------------------------

    implicit none
    integer :: n1,                &! number of equations(=4 for S,E,I,R) 
            & n2,                 &! number of age compartments 
            & max_delay, tot_time,&! max delay and total time of simulation
            & num_vert, max_deg    ! number of vertices of networks & max degree
    real*8  :: dt                  ! time step
    real*8  :: dist                ! geographic distance to calculate delay
    real*8  :: D                   ! scaling parameter for migration
    integer, allocatable :: deg(:), temp_deg(:), init_cities(:) ! self explanatory variables for allocation
    integer, allocatable :: adj_list(:, :), delay(:, :)         ! Adjacency matrix and time delay between i,j
    real*8, allocatable :: eta(:),                               &! migration rate to be scaled with D 
                         & x(:, :),                              &! THE MAIN ARRAY STORING EVERYTHING  
                         & max_time(:),                          &! time at which maxima of infection occurs for all cities
                         & age_groups_fracs(:),                  &! distribution of total fractions in age groups
                         & init_infected_amount(:),              &! initial infected fractions of population
                         & init_exposed_amount(:),               & ! initial exposed fraction of population
                         & pop(:)                                  ! total population
    integer :: i, j, k, ix, m, n
    character (len = 90) :: s


    ! Read the parameters
    ! n1 and n2 are the number of disease compartments and age compartments resp.
    open(unit = 60, file = "parameters.dat")
    read(60, *)dt, n1, n2, num_vert, D, tot_time

    allocate(deg(num_vert), temp_deg(num_vert), pop(num_vert), max_time(num_vert))
    allocate(delay(num_vert, num_vert), eta(num_vert))


    open(unit = 50, file = "populations.dat")
    DO i = 1, num_vert
        read(50, *)j, pop(i)
        pop(i) = pop(i) / 25000.
    ENDDO
    !print*, "maxmin", maxval(pop), minval(pop)
    ! Read the edge list and construct the adjacency list
    deg = 0
    open(unit = 1, file = "edges.txt", status = "old")
    DO m = 1, 1000000
        read(1, *, end = 10) i, j, dist
        deg(i) = deg(i) + 1
        deg(j) = deg(j) + 1
        delay(i, j) = int(dist / (24 * 50 * dt))
        delay(j, i) = delay(i, j)
    ENDDO

10  continue
    !delay = 0
    close(1)

    max_delay = maxval(delay)
    !print*, "max delay", max_delay
    max_deg = maxval(deg)
    !print*, "max_deg", max_deg, n1*n2*num_vert,tot_time, max_delay, tot_time + max_delay

    allocate(adj_list(num_vert, max_deg), x(n1*n2*num_vert, tot_time + max_delay))
    temp_deg = 0

    open(unit = 1, file = "edges.txt", status = "old")
    DO m = 1, 10000000
        read(1, *, end = 20) i, j, dist
        temp_deg(i) = temp_deg(i) + 1
        temp_deg(j) = temp_deg(j) + 1
        adj_list(i, temp_deg(i)) =  j
        adj_list(j, temp_deg(j)) =  i
    ENDDO
    close(1)

20  continue

    ! Specify the initial conditions so that all are susceptibles
    ! specificy fractions of population in each age group. Fractions must sum to 1
    allocate(age_groups_fracs(n2))
    age_groups_fracs = 1./n2
    x = 0
    DO i = 1, n1 * n2 * num_vert, n1 * n2
        DO j = 1, n2
            ! susceptibles are divided into different age groups as per age_groups_fracs
            x(i-1 + j, :) = age_groups_fracs(j) * pop((i-1)/(n1*n2) + 1)
        Enddo
    ENDDO

    ! Expose and infect a few cities initially
    allocate(init_cities(3), init_infected_amount(3), init_exposed_amount(3))
    init_cities = (/1, 4, 5/) ! indices of cities that are to be exposed and infected initially
    init_exposed_amount = (/1e-3, 1e-3, 1e-3/) ! fractions of exposed populations in these cities
    init_infected_amount = (/1e-5, 1e-5, 1e-5/) ! fractions of infected populations in these cities
    DO i = 1, size(init_cities)
        ix = init_cities(i)
        Do j = 1, n2
            ! increase the exposed in age group j
            x((ix-1)*n1*n2 + n2 + j, :) = init_exposed_amount(i) * x((ix-1)*n1*n2 + j, :)
            ! increase the infected in age group j (notice the factor of 2)
            x((ix-1)*n1*n2 + 2*n2 + j, :) = init_infected_amount(i) * x((ix-1)*n1*n2 + j, :)

            ! reduce the susceptibles in age group j
            x((ix-1)*n1*n2 + j, 1) = (1-init_exposed_amount(i)-init_infected_amount(i)) * x((ix-1)*n1*n2 + j, 1)
        Enddo
            
    ENDDO

    open(unit = 2, file = "balance_params.dat")
    DO i = 1, num_vert
        read(2, *)eta(i)
    ENDDO

    ! open output file for infected and exposed populations for all age groups
    do i = 1,n2  
        write (s, '( "Output_data/infected_", I1, ".dat" )' ) i 
        OPEN(unit=100+i,file=s)
        write (s, '( "Output_data/exposed_", I1, ".dat" )' ) i
        OPEN(unit=200+i,file=s)
    end do !fileLoop
    
    !open(unit = 8, file = "Output_data/population_ts.dat")
    !write(8, *)(x(i, max_delay + 1) + x(i+1, max_delay + 1) + x(i+2, max_delay + 1), i = 1, n1*n2 * num_vert, n1*n2)


    ! Call RK4 to integrate the system
    max_time = 0
    call RK4(x, n1, n2, num_vert, dt, max_delay, max_deg, tot_time, pop, deg, eta, delay, max_time, D, adj_list)
    !! Write times at which peaks occur in a file
    open(unit = 51, file = "Output_data/max_times.dat")
    DO i = 1, num_vert
        write(51,*)i, max_time(i)
    ENDDO
    !open(unit = 52, file = "Output_data/max_times_vs_delay.dat")
    !DO i = 1, num_vert
    !    write(52, *)delay(1, i), max_time(i)
    !ENDDO
End Program SEIR

!==================================================================================================
!==================================================================================================

Subroutine RK4(y, n1, n2, num_vert, dt, max_delay, max_deg, tot_time, pop, deg, eta, delay, max_time, D, adj_list)

  !Declare the variables
  implicit none
  integer:: i, j, l, n1, n2, t, tot_time, num_vert, max_deg, deg(num_vert), adj_list(num_vert, max_deg)
  integer :: delay(num_vert, num_vert), max_delay
  real*8 :: y(n1*n2 * num_vert,tot_time+max_delay)
  real*8 :: pop(num_vert), yprime (n1*n2 * num_vert), k(n1*n2 * num_vert,4)
  real*8 D, dt, eta(num_vert), max_time(num_vert*n2)
  
  !y = x ! Very important. This saves initial conditions
  !Integration starts here

  DO t = max_delay + 1, tot_time + max_delay-1
    !print*, t
    write(*,'(I5,A)',ADVANCE='NO') t, CHAR(13)
!    write(*,'(1a1,<type>,$)') char(13), t
!999 format (I5, TL4)    

    call derivative(n1, n2, num_vert, t, tot_time, max_deg, y, yprime, delay, pop, deg, eta, D, adj_list,max_delay)
    k(:, 1) = yprime(:)
    !print*, sum(k(:, 1))
    y(:, t+1) = y(:, t) + 0.5 * k(:, 1) * dt
  
    call derivative(n1, n2, num_vert, t, tot_time, max_deg, y, yprime, delay, pop, deg, eta, D, adj_list,max_delay)
    k(:, 2) = yprime(:)
    y(:, t+1) = y(:, t) + 0.5 * k(:, 2) * dt

   call derivative(n1, n2, num_vert, t, tot_time, max_deg, y, yprime, delay, pop, deg, eta, D, adj_list,max_delay)
    k(:, 3) = yprime(:)
    y(:, t+1) = y(:, t) + k(:, 3) * dt

   call derivative(n1, n2, num_vert, t, tot_time, max_deg, y, yprime, delay, pop, deg, eta, D, adj_list,max_delay)
    k(:, 4) = yprime(:)

   y(:, t+1) = y(:, t) + (1/6.) * (k(:,1) + 2. * k(:,2) + 2. * k(:,3) + k(:,4)) * dt

    ! Detect the maximum for total infected
    Do i = 2*n2+1, n1*n2*num_vert, n1*n2
        if (sum(y(i:i+n2-1, t+1)) > sum(y(i:i+n2-1, t)))then
            max_time((i-2*n2-1)/(n1*n2)+1) = (t-max_delay) * dt
        endif
    Enddo

    ! Write the output for all the nodes
    if (mod(t,10) == 0 ) then
        Do j = 1, n2
            write(100 + j, *)(t-max_delay-1)*dt, (y(l, t), l = 2*n2+j, n1*n2 * num_vert, n1*n2)
            write(200 + j, *)(t-max_delay-1)*dt, (y(l, t), l = n2+j, n1*n2 * num_vert, n1*n2)
        Enddo
    endif
   ! Write the total populations 
   !write(8, *)(y(i, t) + y(i+1, t) + y(i+2, t), i = 1, n1*n2 * num_vert, n1*n2)
  ENDDO !Integration ends here

End Subroutine RK4

subroutine derivative(n1, n2, num_vert, t, tot_time, max_deg, y, yprime, delay, pop, deg, eta, D, adj_list,max_delay)
    implicit none
    integer i, j, k, n1, n2, t, nbr, num_vert, max_deg, deg(num_vert), adj_list(num_vert, max_deg)
    integer :: delay(num_vert, num_vert), vert_ind, tot_time,max_delay, delta
    real*8 :: beta, theta, D, nu, s, e_by_d, eta(num_vert), pop(num_vert)
    real*8 :: y(n1*n2 * num_vert, tot_time+max_delay), yprime(n1*n2 * num_vert), coupling(n1*n2)
     
    ! Parameter values
    beta = 0.2; theta = 0.07; nu = 0.2
    !print *, "Reproduction number R0= ", beta/theta 
  
    DO i = 1, n1*n2 * num_vert, n1*n2
        ! compute the coupling term
        coupling = 0
        vert_ind =  (i-1)/(n1*n2) + 1
        ! This loop runs over neighbours
        Do j = 1, deg(vert_ind)
            nbr = adj_list(vert_ind, j)
            delta= delay(vert_ind, nbr)
            e_by_d = eta(nbr)/deg(nbr)
            ! This loop is over age compartments
            do k = 1, n2
                coupling(k)        = coupling(k)      +  y((nbr-1) * n1*n2 + k,  t -delta) * e_by_d 
                coupling(k + n2)   = coupling(k+n2)   +  y((nbr-1) * n1*n2 + n2 + k, t -delta)*e_by_d 
                coupling(k + 2*n2) = coupling(k+2*n2) +  y((nbr-1) * n1*n2 + 2*n2 + k,t -delta)*e_by_d
                coupling(k + 3*n2) = coupling(k+3*n2) +  y((nbr-1) * n1*n2 + 3*n2 + k,t -delta)*e_by_d 
            enddo
            !print*, coupling(1), coupling(2), coupling(3), vert_ind , j
        Enddo

        Do j = 1, n2 ! this index runs over age compartments
            s = 0
            do k = 1, n2 ! this index also runs over age compartments
                s = s + y(i-1 + 2*n2 + k, t)
            enddo
            yprime(i-1+j)   = -beta/pop(vert_ind) * y(i-1+j, t) * s &
                        & - D * (eta(vert_ind) * y(i-1+j, t) - coupling(j))
            yprime(i-1+n2+j) = beta/pop(vert_ind) * y(i-1+j, t) * s - nu * y(i-1+n2+j, t) &
                        & - D * (eta(vert_ind) * y(i-1+n2+j, t) - coupling(j+n2))
            yprime(i-1+2*n2+j) = nu * y(i-1+n2+j, t) - theta * y(i-1+2*n2+j, t) & 
                        & - D * (eta(vert_ind) * y(i-1+2*n2+j, t) - coupling(j+2*n2))
            yprime(i-1+3*n2+j) = theta * y(i-1+2*n2+j, t) & 
                        & - D * (eta(vert_ind) * y(i-1+3*n2+j, t) - coupling(j+3*n2))
        Enddo
    ENDDO
end subroutine derivative
