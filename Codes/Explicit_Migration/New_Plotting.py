#!/usr/bin/env python
# coding: utf-8

# In[151]:


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import itertools
from bokeh.io import output_file, show,curdoc
from bokeh.models import Dropdown,Select
from bokeh.plotting import figure
from bokeh.layouts import row,column
from bokeh.models import HoverTool
from bokeh.palettes import Spectral11


# In[122]:


def rename_cities(df,cities):
    return df.rename(columns = {i+1 : cities[i] for i in range(len(cities))}, inplace = True)


# In[173]:


sus_df=pd.read_csv("Output_data/susceptibles_9d.dat",sep="\s+",header=None)
expos_df=pd.read_csv("Output_data/exposed_9d.dat",sep="\s+",header=None)
asymp_df=pd.read_csv("Output_data/asympt_9d.dat",sep="\s+",header=None)
presymp_df=pd.read_csv("Output_data/presympt_9d.dat",sep="\s+",header=None)
symp_min_df=pd.read_csv("Output_data/sympt_minor_9d.dat",sep="\s+",header=None)
symp_sev_df=pd.read_csv("Output_data/sympt_severe_9d.dat",sep="\s+",header=None)
hos_df=pd.read_csv("Output_data/hospitalized_9d.dat",sep="\s+",header=None)
rec_df=pd.read_csv("Output_data/recovered_9d.dat",sep="\s+",header=None)
death_df=pd.read_csv("Output_data/died_9d.dat",sep="\s+",header=None)


# In[174]:


cities = pd.read_csv("vert_index_with_names.dat", sep = "\s+")
cities = list(cities["name"])
cities = [name.title() for name in cities]

for df in [sus_df,expos_df,asymp_df,presymp_df,symp_min_df,symp_sev_df,hos_df,rec_df,death_df]:
    rename_cities(df,cities)

colors=[]
for name, color in zip(cities, itertools.cycle(Spectral11)):
    colors.append(color)


# In[175]:


#fig = plt.figure(figsize=(10,8))
x=expos_df[0]
#ax.plot(x,sus_df[1],linewidth=2, label="Succeptible")
selected_cities=["Delhi", "Mumbai", "Chennai", "Kolkata","Pune","Bengaluru", "Hyderabad"]
cityplots=['']*len(selected_cities)
for city in  selected_cities:
    idx=selected_cities.index(city)
    #ax  = fig.add_subplot(len(selected_cities),1,selected_cities.index(city)+1)
    #plt.plot(df[0], df[city], '--')
    #plt.fill(df[0], df[city],alpha=.2,label='')
    #ax.plot(x,expos_df[city],linewidth=2, label="Exposed")
    cityplots[idx]=figure(plot_width=800, plot_height=200, background_fill_color="#fafafa")
    cityplots[idx].line(x,expos_df[city],line_width=3,legend_label="Exposed",color=colors[0])
    cityplots[idx].line(x,asymp_df[city],line_width=3,legend_label="Asymptomatic",color=colors[1])
    cityplots[idx].line(x,presymp_df[city],line_width=3,legend_label="Pre-symptomatic",color=colors[2])
    cityplots[idx].line(x,symp_min_df[city],line_width=3,legend_label="Mild symptomatic",color=colors[3])
    cityplots[idx].line(x,symp_sev_df[city],line_width=3,legend_label="Severe symptomatic",color=colors[4])
    cityplots[idx].line(x,hos_df[city],line_width=3,legend_label="Hospitalized",color=colors[5])
    #cityplots[idx].line(x,rec_df[city],line_width=3,legend_label="Recovered",color=colors[6])
    cityplots[idx].line(x,death_df[city],line_width=3,legend_label="Death",color=colors[7])
    
    #plt.ylabel("x*25000",fontsize=15)
    #plt.xlabel("t in days",fontsize=15)
    #plt.xlim(0,200)
    #plt.legend(fontsize=15)
    #plt.savefig("picture.pdf")
    cityplots[idx].title.text=city
    cityplots[idx].legend.click_policy="hide"

show(column(cityplots,sizing_mode="scale_width"))


# In[ ]:




