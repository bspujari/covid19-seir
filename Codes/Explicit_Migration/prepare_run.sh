#!/bin/bash

function show_err()
{
	echo "Sorry! Incorrect argument"
	echo "You need to select what type of run you want to set up."
	echo "Available options are ANY ONE of the following:"
	echo "States,states,state,S,s, Districts,districts,dist, D, d, Cities, City, cities, city, C, c"
	exit 1
}

function parameter_file()
{
cat > parameters.dat <<EOF
# lines containing '#'s will be ignored
# One variable on a line. '!' needs to be preceded with a whitespace
$1 ! num_vert
0.1 ! dt
9 ! dfs
6  ! age compartments
0.1  ! D-migration stregnth
3000 ! number of iterations
1 ! number of initially infected cities
0 ! lockdown begins on this day
0 ! .. and ends on this
1 ! beta strength 
0.00  ! Q_tau 
EOF
}



if [ "$#" -gt 0 ] ; then
	opt=$1
	case ${opt} in
		state | states | States |State  | S | s ) cat='states' ;; # 
		city | cities| City | Cities | C | c ) cat='cities' ;; 
		dist | districts | Districts | D| d ) cat='districts';;
		* ) show_err ;;
	esac
else
	echo "Needs one argument: states|districts|cities"
	exit 1
fi

printf "This script will erase all your input files. Are you sure you want to continue? (y/N): "

read ans
if [ $ans == 'y' ] ; then 
	echo "Preparing the run for $cat"

	rm -f parameters.dat  pop_region_type.dat populations.dat edges.txt balance_params.dat vert_index_with_names.dat

	if [ $cat == 'states' ] ; then
		parameter_file 32
		src_dir="States_files/"
	elif [ $cat == 'cities' ] ; then
		parameter_file 318
		src_dir="Cities_files/"
	elif [ $cat == 'districts' ] ; then
		parameter_file 35
		echo "Warning: Only Maharashtra's data is copied at this moment" 
		src_dir="District_files/Maharashtra/"
		cat='maha'
		ln -s $src_dir/pop_region_type_maha.dat pop_region_type.dat 
	fi

	if [ ! -f $src_dir/balance_params_$cat.dat ]; then echo "ERROR. balanceparams missing"; fi
	ln -s $src_dir/balance_params_$cat.dat  balance_params.dat
	if [ ! -f $src_dir/edges_$cat.txt ]; then echo "ERROR. edges missing"; fi
	ln -s $src_dir/edges_$cat.txt edges.txt 
	if [ ! -f $src_dir/populations_$cat.dat ]; then echo "ERROR. populations missing"; fi
	ln -s $src_dir/populations_$cat.dat  populations.dat   
	if [ ! -f $src_dir/vert_index_with_names.dat ]; then echo "ERROR. vert_index missing"; fi
	ln -s $src_dir/vert_index_with_names.dat .

	echo "Preliminary set up has been prepared. Please check all the input files before you begin the production runs."
fi # ans if-loop
