import os
import subprocess
import sys


def get_beta(R0):
    alpha = 0.67; Ca = 0.67; Cp = 1; Lp = 0.5; Lm = 0.1428
    Ls = 0.1736; mu = 0.956; La = 0.1428

    return R0 / (alpha*(Ca/La) + (1-alpha)*(1/Lp + mu/Lm + (1-mu)/Ls))

def get_R0(beta):
    alpha = 0.67; Ca=0.67; Cp=1; Lp = 0.5; Lm = 0.1428
    Ls = 0.1736; mu = 0.956; La = 0.1428

    return beta * (alpha*(Ca/La) + (1-alpha)*(1/Lp + mu/Lm + (1-mu)/Ls))

string=''
if len(sys.argv) > 1 : string=sys.argv[1]


print ("Added string: "+string) 
# Parameters

RL = 0.95
#RL = 1.05
#RL = 1.27
num_vert = 15
dt = 0.05
age_comparts = 6
wgrp = 3
D = 0.1
tot_t = 4000
init_infect = 15
num_intv = 2

# Lockdown parameters
lcdn_types = [1, 3]
begin_lcdn = [0, 48]
end_lcdn = [49, 300]
work_days = [0, 0] # Only for periodic lockdown
period = [0, 0] # Only for periodic lockdown
work_stag = [0, 5]
work_lcdn_stag = [0, 7]

pop = 7090000# Population of the infected city


if init_infect == 1:
    with open('init_conditions.dat', 'w') as f:
        f.write(f"1 {15000/pop} {3000/pop} {6000/pop} \n")

for Q_tau in [0.0, 0.0033]:
    for R0 in [1.2, 1.6, 2.0, 2.4]:
    
        lcdn_val = get_beta(RL)/get_beta(R0)
        print(R0, get_beta(R0), lcdn_val)
        # Create the parameter file
        with open('parameters.dat', 'w') as f:
            f.write("# Number of nodes: \n")
            f.write(f"{num_vert}\n")
            f.write(f"# time step  \n")
            f.write(f"{dt}\n")
            f.write(f"# number of compartments. 9 do not change\n")
            f.write(f"{9}\n")
            f.write(f"# Num of age compartments. \n")
            f.write(f"{age_comparts}\n")
            f.write(f"# Working classes. (>1 useful only with staggered lockdown "
                    "(see below)) \n")
            f.write(f"{wgrp}\n")
            f.write(f"# Migration strength \n")
            f.write(f"{D}\n")
            f.write(f"# Number of iterations\n")
            f.write(f"{tot_t}\n")
            f.write(f"#Number of initially infected cities\n")
            f.write(f"{init_infect}\n")
            f.write(f"! Q_tau\n")
            f.write(f"{Q_tau}\n")
            f.write(f"# Interventions:! Number of interventions: 0,1 or 2\n")
            f.write(f"{num_intv}\n")
            f.write(f"# Space separated array. ex: 0 1 .  0: No-lockdown, "
              "1: Simple lockdown, 2: Periodic lockdown, 3: Staggered lockdown\n")
            f.write(f"{lcdn_types[0]} {lcdn_types[1]}\n")
            f.write(f"! Space separated array of first days of lockdown. "
                "ex: 10 20 .. first lockdown on day 10 and second on 20\n")
            f.write(f"{begin_lcdn[0]} {begin_lcdn[1]}\n")
            f.write(f"! Space separated array of last days of  lockdown. ex: "
                "20 30 .. first lockdown on day 10 and second on 20\n")
            f.write(f"{end_lcdn[0]} {end_lcdn[1]}\n")
            f.write(f"! Space separated array of working days of PERIODIC "
                "lockdown.\n")
            f.write(f"{work_days[0]} {work_days[1]}\n")
            f.write(f"! Space separated array of working days+lockdown days of "
                "PERIODIC lockdown.\n")
            f.write(f"{period[0]} {period[1]}\n")
            f.write(f"! Space separated array of working days of STAGGERED "
                "lockdown.\n")
            f.write(f"{work_stag[0]} {work_stag[1]}\n")
            f.write(f"! Space separated array of working days+lockdown days of "
                "STAGGERED lockdown.\n")
            f.write(f"{work_lcdn_stag[0]} {work_lcdn_stag[1]}\n")
            f.write(f"! scaling factor for beta during (all types of) lockdown \n")
            f.write(f"{lcdn_val}\n")
        
        # Create the model parameter file
        with open('model_parameters.dat', 'w') as f:
            f.write(f"{get_beta(R0)}\n")
            f.write(f"0.1386\n")
            f.write(f"0.2814\n")
            f.write(f"0.5\n")
            f.write(f"0.1428\n")
            f.write(f"0.1736\n")
            f.write(f"0.1428\n")
            f.write(f"0.068\n")
        
        subprocess.call("./a.out")
    
        # Rename files
        if Q_tau > 0:
            test = "_test"
        else:
            test = "_no_test"
        for i in range(1, wgrp + 1):
            # Total infected on all vertices
            os.rename(f"Output_data/total_infected_work_{i}.dat", 
                f"Output_data/infect_work{i}_Rw{R0}_RL{RL}_D{D}{test}"+string+".dat")
            # Total hospitalized on all vertices
            os.rename(f"Output_data/hospitalized_work_{i}.dat", 
                f"Output_data/hosp_work{i}_Rw{R0}_RL{RL}_D{D}{test}"+string+".dat")
    

