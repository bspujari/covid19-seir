Module equations
    implicit none

    ! Variable names and indices:
    ! 1 : susceptible (s), 2 : exposed (e), 3 : pre-symptomatic (ip), 4: asymptomatic (ia), 5 : symptomatic,severe (is)
    ! 6 : symptomatic,minor (im), 7 : hospitalized (h), 8 : recovered (r), 9 : dead (d)
    ! parameters names should be self-explanatory now
    !real*8 :: se, eip, eia, ipis, ipim, iar, ish, imr, hr, hd, isd, isr, lcdn_val,q_tau
    real*8 :: se, eip, eia, lambda_p, iar, lambda_s, imr, rho, lcdn_val, tau, tau_lcdn
    !real*8 , allocatable :: age_rate_factor(:),age_death_factor(:)
    integer*8 :: num_intv
    integer*8, allocatable :: lcdn_type(:), begin_lcdn(:), end_lcdn(:), work_period(:),&
            &lcdn_period(:), stag_period(:), stag_workdays(:)
    integer*8, allocatable :: region_type(:)
    
    contains
    Subroutine derivative(n1, n2, n3, num_vert, t,dt, tot_time, max_deg, max_delay, deg, &
        & x, xprime, delay, pop, eta, D_orig, adj_list)
        implicit none
        integer*8 :: val, i, j, jj, k, kk, ix, n1, n2, n3, num_vert, t, tot_time, nbr, delta, max_deg, max_delay
        integer*8 :: wgrp
        integer*8 :: deg(num_vert), delay(num_vert, num_vert), adj_list(num_vert, max_deg)
        real*8 :: eta_by_d, tot_infect, D, D_orig, dt, hd, hr, ipis, ipim, isd, ish, isr, factor, q_tau
        real*8 :: age_factor(n2), age_death_factor(n2), urban(3), rural(3), beta(n2*n3, n2*n3)
        real*8 :: x(n1*n2*n3*num_vert, tot_time + max_delay), xprime(n1*n2*n3*num_vert)
        real*8 :: pop(num_vert), eta(num_vert), coupling(n1*n2*n3)

        beta=se
        D = D_orig
        q_tau = 1./tau
        ! lockdown
        DO ix = 1, num_intv
            if (t*dt > begin_lcdn(ix) .and. t*dt < end_lcdn(ix))then
                ! Change the tau
                q_tau = 1./tau_lcdn
                ! Reduce the migration
                D = D_orig/10.
                if (lcdn_type(ix) == 1) then
                    ! single complete lockdown
                    beta = se * lcdn_val
                elseif (lcdn_type(ix) == 2) then
                    ! periodic lockdown
                    if (mod(int(t*dt-begin_lcdn(ix), 8), lcdn_period(ix)) < work_period(ix)) then    
                        beta = se
                    else
                        beta = se*lcdn_val
                    endif
                elseif (lcdn_type(ix) == 3) then
                    ! staggered lockdown
                    beta = se*lcdn_val
                       ! Decide the working group based on the week
                       wgrp = mod(int(t*dt,8), stag_period(ix)*n3)/ stag_period(ix)  
                       ! Allow the working group to work on proper days of the week
                       if (mod(int(t*dt,8), stag_period(ix)) <  stag_workdays(ix))then
                           DO j = 1, n2
                               Do k = 1, n2
                                   beta((j-1)*n3+wgrp+1, (k-1)*n3+wgrp+1) = se
                               Enddo
                           ENDDO
                       endif
                endif
            !else
            !    ! Make the migration normal
            !    D = D_orig
            !    ! Make the contact normal 
            !    beta = se 
            !    print *, "ELSE", t*dt, se,  begin_lcdn(ix),  end_lcdn(ix)
          endif
        ENDDO

        if (n2 .eq. 7) then 
                !age_factor = (/0.012, 0.049, 0.102, 0.166, 0.243, 0.273/)
                !age_death_factor = (/0.0003, 0.0015, 0.006, 0.022, 0.051, 0.093/)
                !age_factor = (/0.012, 0.012, 0.049, 0.108, 0.166, 0.243, 0.273/)
                age_factor = (/0.988, 0.988, 0.951, 0.892, 0.834, 0.757, 0.727/)
                age_death_factor = (/0.025, 0.025, 0.03, 0.055, 0.132, 0.2, 0.34/)
        else
                age_factor = 0.956
                age_death_factor= 0.2
        endif

        !urban = (/0.8, 0.15, 0.05/)
        !rural = (/0.5, 0.4, 0.1/)
        urban = (/1.,0.,0./)
        rural = (/1.,0.,0./)
    
        DO i = 1, n1*n2*n3 * num_vert, n1*n2*n3
            ! get vertex index
            ix = (i-1)/(n1*n2*n3) + 1
            if (region_type(ix) == 1)then
                ish = lambda_s * urban(1)
                isr = lambda_s * urban(2)
                isd = lambda_s * urban(3)
            else
                ish = lambda_s * rural(1)
                isr = lambda_s * rural(2)
                isd = lambda_s * rural(3)
            endif

            ! Calculate the couplings
            coupling = 0
            if (D /= 0)then
                Do j = 1, deg(ix)
                    nbr = adj_list(ix, j)
                    eta_by_d = eta(nbr) / deg(nbr)
                    delta = delay(ix,nbr)
                    do k = 1, n2 ! age groups
                        do jj = 1, n3 ! working classes
                            coupling((k-1)*n3+jj)      = coupling((k-1)*n3+jj)      &
                                & + eta_by_d * x((nbr-1)*n1*n2*n3+(k-1)*n3+jj,t-delta)
                            coupling((k-1)*n3+n2*n3+jj)   = coupling((k-1)*n3+n2*n3+jj)   &
                                & + eta_by_d * x((nbr-1)*n1*n2*n3+(k-1)*n3+1*n2*n3+jj,t-delta)
                            coupling((k-1)*n3+2*n2*n3+jj) = coupling((k-1)*n3+2*n2*n3+jj) &
                                & + eta_by_d * x((nbr-1)*n1*n2*n3+(k-1)*n3+2*n2*n3+jj,t-delta)
                            coupling((k-1)*n3+3*n2*n3+jj) = coupling((k-1)*n3+3*n2*n3+jj) & 
                                & + eta_by_d * x((nbr-1)*n1*n2*n3+(k-1)*n3+3*n2*n3+jj,t-delta)
                            coupling((k-1)*n3+4*n2*n3+jj) = coupling((k-1)*n3+4*n2*n3+jj) &
                                & + eta_by_d * x((nbr-1)*n1*n2*n3+(k-1)*n3+4*n2*n3+jj,t-delta)
                            coupling((k-1)*n3+5*n2*n3+jj) = coupling((k-1)*n3+5*n2*n3+jj) &
                                & + eta_by_d * x((nbr-1)*n1*n2*n3+(k-1)*n3+5*n2*n3+jj,t-delta)
                            coupling((k-1)*n3+6*n2*n3+jj) = coupling((k-1)*n3+6*n2*n3+jj) &
                                & + eta_by_d * x((nbr-1)*n1*n2*n3+(k-1)*n3+6*n2*n3+jj,t-delta)
                            coupling((k-1)*n3+7*n2*n3+jj) = coupling((k-1)*n3+7*n2*n3+jj) &
                                & + eta_by_d * x((nbr-1)*n1*n2*n3+(k-1)*n3+7*n2*n3+jj,t-delta)
                            coupling((k-1)*n3+8*n2*n3+jj) = coupling((k-1)*n3+8*n2*n3+jj) &
                                & + eta_by_d * x((nbr-1)*n1*n2*n3+(k-1)*n3+8*n2*n3+jj,t-delta)
                        enddo
                    enddo
                Enddo
            endif
            ! sum all the infected population across all age groups, working classes and disease compartments
            !tot_infect = 0.
            !Do j = 1, n2 ! This loop is over age compartments
            !    do jj = 1, n3
            !        tot_infect = tot_infect + x(i-1+(j-1)*n2*n3+2*n3+jj,t) + (2./3.) * x(i-1+(j-1)*n2*n3+3*n3+jj,t) &
            !        & + x(i-1+(j-1)*n2*n3+4*n3+jj,t) + x(i-1+(j-1)*n2*n3+5*n3+jj,t)
            !    enddo
            !Enddo
            !tot_infect = exp(-q_tau*(t-max_delay)*dt)*tot_infect

            !if (t*dt < 5)then
            !    q_tau = 1./360
            !elseif (t*dt >= 72 .and. t*dt < 91)then
            !    q_tau = 1./360
            !elseif (t*dt > 132)then
            !    q_tau = 1./360
            !else
            !    q_tau = 1./150
            !endif
            Do j = 1, n2 ! This loop is over age compartments

                ipis = lambda_p * (1-age_factor(j))
                ipim = lambda_p * age_factor(j)
                hd   = rho * age_death_factor(j)
                hr   = rho * (1-age_death_factor(j))

                do jj = 1, n3 ! This loop is over working classes

                    ! sum all the infected population across all age groups, working classes and disease compartments
                    tot_infect = 0.
                    Do k = 1, n2 ! This loop is over age compartments
                        do kk = 1, n3
                            tot_infect = tot_infect + beta((j-1)*n3+jj, (k-1)*n3+kk) &
                            & * (exp(-q_tau* (t-max_delay)*dt) * x(i-1+(k-1)*n3+2*n2*n3+kk,t) &
                            & + exp(-0.5*q_tau* (t-max_delay)*dt) * (2./3.) * x(i-1+(k-1)*n3+3*n2*n3+kk,t) &
                            & + exp(-q_tau* (t-max_delay)*dt) * x(i-1+(k-1)*n3+4*n2*n3+kk,t) & 
                            & + exp(-q_tau* (t-max_delay)*dt) * x(i-1+(k-1)*n3+5*n2*n3+kk,t))
                        enddo
                    Enddo
                    !tot_infect = exp(-q_tau*(t-max_delay)*dt)*tot_infect
                    ! susceptibles
                    xprime(i-1+(j-1)*n3+jj)   = - (1./pop(ix)) * x(i-1+(j-1)*n3+jj,t) * tot_infect  & 
                        & - D * (eta(ix) * x(i-1+(j-1)*n3+jj,t) - coupling((j-1)*n3+jj))  

                    ! exposed
                    xprime(i-1+(j-1)*n3+n2*n3+jj) = (1./pop(ix)) * x(i-1+(j-1)*n3+jj,t) * tot_infect &
                        &- eip *x(i-1+(j-1)*n3+n2*n3+jj,t) - eia *x(i-1+(j-1)*n3+n2*n3+jj,t) &
                        & - D * (eta(ix) * x(i-1+(j-1)*n3+n2*n3+jj,t) - coupling((j-1)*n3+n2*n3+jj))

                    ! pre-symptomatic
                    xprime(i-1+(j-1)*n3+2*n2*n3+jj) = eip *x(i-1+(j-1)*n3+n2*n3+jj,t)&
                        & - ipis *x(i-1+(j-1)*n3+2*n2*n3+jj,t) - ipim *x(i-1+(j-1)*n3+2*n2*n3+jj,t) &
                        & - D * (eta(ix) * x(i-1+(j-1)*n3+2*n2*n3+jj,t) - coupling((j-1)*n3+2*n2*n3+jj))

                    ! asymptomatic
                    xprime(i-1+(j-1)*n3+3*n2*n3+jj) = eia *x(i-1+(j-1)*n3+n2*n3+jj, t) &
                        & - iar *x(i-1+(j-1)*n3+3*n2*n3+jj,t) &
                        & - D * (eta(ix) * x(i-1+(j-1)*n3+3*n2*n3+jj,t) - coupling((j-1)*n3+3*n2*n3+jj))

                    ! symptomatic-severe
                    xprime(i-1+(j-1)*n3+4*n2*n3+jj) = ipis *x(i-1+(j-1)*n3+2*n2*n3+jj,t) &
                        & - ish *x(i-1+(j-1)*n3+4*n2*n3+jj,t) - isr * x(i-1+(j-1)*n3+4*n2*n3+jj,t) &
                        & - isd * x(i-1+(j-1)*n3+4*n2*n3+jj,t) &
                        & - D * (eta(ix) * x(i-1+(j-1)*n3+4*n2*n3+jj,t) - coupling((j-1)*n3+4*n2*n3+jj))

                    ! symptomatic-minor
                    xprime(i-1+(j-1)*n3+5*n2*n3+jj) = ipim*x(i-1+(j-1)*n3+2*n2*n3+jj,t) &
                        & - imr *x(i-1+(j-1)*n3+5*n2*n3+jj,t) &
                        & - D * (eta(ix) * x(i-1+(j-1)*n3+5*n2*n3+jj,t) - coupling((j-1)*n3+5*n2*n3+jj))

                    ! hospitalized
                    xprime(i-1+(j-1)*n3+6*n2*n3+jj) = ish *x(i-1+(j-1)*n3+4*n2*n3+jj,t) &
                        & - hr *x(i-1+(j-1)*n3+6*n2*n3+jj,t) - hd * x(i-1+(j-1)*n3+6*n2*n3+jj,t) &
                        & - D * (eta(ix) * x(i-1+(j-1)*n3+6*n2*n3+jj,t) - coupling((j-1)*n3+6*n2*n3+jj))

                    ! recovered
                    xprime(i-1+(j-1)*n3+7*n2*n3+jj) = iar *x(i-1+(j-1)*n3+3*n2*n3+jj,t)&
                        & + imr *x(i-1+(j-1)*n3+5*n2*n3+jj,t) + hr *x(i-1+(j-1)*n3+6*n2*n3+jj,t) &
                        & + isr * x(i-1+(j-1)*n3+4*n2*n3+jj,t) &
                        & - D * (eta(ix) * x(i-1+(j-1)*n3+7*n2*n3+jj,t) - coupling((j-1)*n3+7*n2*n3+jj))

                    ! dead
                    xprime(i-1+(j-1)*n3+8*n2*n3+jj) = hd * x(i-1+(j-1)*n3+6*n2*n3+jj,t) &
                       & + isd * x(i-1+(j-1)*n3+4*n2*n3+jj,t)

                enddo
            Enddo
        ENDDO
    End subroutine derivative
End module equations
