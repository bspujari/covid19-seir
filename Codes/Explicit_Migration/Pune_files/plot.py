import pandas as pd
import matplotlib.pyplot as plt

comparts = ['asympt', 'exposed', 'recovered', 'sympt_minor', 'died', 
    'hospitalized', 'presympt', 'susceptibles', 'sympt_severe']

compart = 'hospitalized'

ward_names = ['Shivajinagar-Ghole Road', 'Kasaba-Vishrambagwada', 
    'Wanowrie Ramtekdi', 'Sinhgad Road', 'Dhankawadi Sahakarnagar', 
    'Bibwewadi', 'Hadapsar-Mundhwa', 'Bhavani Peth', 'Kondhwa-Yewalewadi', 
    'Yerawada-Kala-Dhanori', 'Warje Karvenagar', 'Kothrud Bavdhan', 
    'Aundh Baner', 'Nagar Road-Wadgaonsheri', 'Dhole Patil Road', ]

path = 'Output_data/' + compart + '_9d.dat'
df = pd.read_csv(path, sep = '\s+', header = None)
for i, name in enumerate(ward_names):
    plt.plot(df[0], df[i+1], label = name)
    plt.legend()

plt.show()

