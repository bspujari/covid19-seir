import sys
import numpy as np
import graph_tool.all as gt

g = gt.load_graph("pune_wards.gt")
print(g)
population = g.new_vp("int")
balance_param = g.new_vp("float")
new_balance_param = g.new_vp("float")

with open("populations.dat") as f:
    for line in f: 
        v, pop = line.split()
        if v.isnumeric() :# skip the non numeric line
            population[int(v)-1] = int(pop) / 25000
        else:
            print ("Skipped: ", line)

# Initial populations and balance parameters
for v in g.vertices():
    balance_param[v] = 1

for t in range(1000):
    sys.stdout.write(f"\rt = {t}")
    sys.stdout.flush()
    # Update the balance parameters of all vertices
    for v in g.vertices():
        # Get the neighbours of v
        nbrs = g.get_out_neighbours(v)
        new_balance_param[v] = 0
        for nbr in nbrs:
            new_balance_param[v] += balance_param[nbr] * population[nbr] / g.vertex(nbr).out_degree()
        try:
            new_balance_param[v] = new_balance_param[v] / population[v]
        except ZeroDivisionError:
            print(v, population[v])
            sys.exit()

    # Check for the convergence
    if max(abs(balance_param.a - new_balance_param.a)) < 10**(-6):
        break        
        
    balance_param.a = new_balance_param.a

# Save converged balance parameters
np.savetxt("balance_params_pune_wards.dat", balance_param.a, fmt = "%f")

