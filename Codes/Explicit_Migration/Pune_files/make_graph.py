import pandas as pd
import graph_tool.all as gt

df = pd.read_csv('edges.txt', sep = '\s+', header = None)

pop = pd.read_csv('vert_index_with_names.dat', names = ['name', 'code', 'pop'])
print(pop['name'])
g = gt.Graph(directed = False)
g.add_edge_list(df.values-1)
gt.remove_parallel_edges(g)
print(g)

name = g.new_vp('string')
population = g.new_vp('int')
print(pop['name'])
for v, ward in zip(g.vertices(), list(pop['name'])):
    name[v] = ward
    #pop[v] = tot

g.vp['name'] = name
#g.vp['pop'] = pop
g.save('pune_wards.gt')
gt.graph_draw(g, vertex_text = name, vertex_text_position = -2, vertex_text_color = 'k')
