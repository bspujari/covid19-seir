import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_excel('pune_covid_numbers.xlsx')
first_row = df.iloc[0]
print(df)
print(first_row)
df = df.diff()
df.iloc[0] = first_row
df.drop('Day', axis = 1, inplace = True)
df = df.astype('int')
print(df)

ward_names = ['Shivajinagar-Ghole Road', 'Kasaba-Vishrambagwada', 'Wanowrie Ramtekdi', 'Sinhgad Road', 'Dhankawadi Sahakarnagar', 'Bibwewadi', 'Hadapsar-Mundhwa', 'Bhavani Peth', 'Kondhwa-Yewalewadi', 'Yerawada-Kala-Dhanori', 'Warje Karvenagar', 'Kothrud Bavdhan', 'Aundh Baner', 'Nagar Road-Wadgaonsheri', 'Dhole Patil Road', 'Outside Pune']


for i, area in zip(range(16), ward_names):
    print(i, area)
    #x = df.loc[i]
    plt.plot(list(df.iloc[i]), "o-", label = area)
    plt.legend()
plt.show()
