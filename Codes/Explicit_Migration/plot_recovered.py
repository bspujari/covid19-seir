import pandas as pd
import matplotlib.pyplot as plt


#df = pd.read_csv('Output_data/recovered_9d.dat', sep = '\s+', header = None)
df = pd.read_csv('Output_data/susceptibles_9d.dat', sep = '\s+', header = None)
print(df.head())
names = pd.read_csv('vert_names_with_index.dat', sep = '\s+', header = None)
print(names.head())

for i, name in zip(range(1, 119), names[1][0:119]):
    plt.plot(df[0], df[i], label = f'{name}', lw = i/10)
    plt.legend()

plt.show()
