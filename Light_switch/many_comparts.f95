Program many_comparts
    use rk4_integrate
    implicit none
    integer*8 :: i, j, u, v, n1, n2, ix, num_vert, tot_time, max_deg, max_delay, tot_init_infected
    !begin/end_lcdn : the day on which the lockdown begins/ends. 
    integer*8, allocatable :: deg(:), delay(:, :)
    integer*8, allocatable :: init_cities(:), adj_list(:, :)
    real*8 :: dt, D, dist
    real*8, allocatable :: x(:, :) , age_groups_fracs(:), pop(:), eta(:)
    real*8, allocatable :: init_exposed_amount(:), init_sym_amount(:), init_asym_amount(:)
    real*8 :: read_line

    ! Read the parameters
    open(unit = 1, file = 'parameters.dat')
    read(1, *)num_vert, dt, n1, n2, D, tot_time, tot_init_infected, &
            & begin_lcdn, end_lcdn, lcdn_val,q_tau
    close(1)
    allocate(deg(num_vert), delay(num_vert, num_vert), pop(num_vert), eta(num_vert))

    open(unit = 10, file = 'model_parameters.dat') ! parameters specific to virus
    read(10, *)se_orig, eip, eia, ipis, ipim, iar, ish, imr, hr, hd
    close(10)
    ! Rates corresponding to E -> (Ia & Ip) can be written as : 
    !           E -> ei*alpha -> Ip
    !           E -> ei*(1-alpha) -> Ia
    ! Therefore in input file eia = ei*alpha and eip = ei*(1-alpha)  

    ! Similarly if ii is the rate of transtion from Ip to (Im & Is) then
    !           Ip -> ii*mu -> Im
    !           Ip -> ii*(1-mu) -> Is 
    ! Threfore in input file ipis = ii*mu and ipim=ii*(1-mu) 

    ! Finally H -> (R & D) :
    !           H -> rho*delta -> D
    !           H -> rho*(1-delta) -> R
    ! Therfore in input file hr= rho*(1-delta) and hd=rho*delta
   
    ! Read the populations
    open(unit = 1, file = 'populations.dat')
    DO i = 1, num_vert
        read(1, *)u, v
        pop(i) = v / 25000. 
    ENDDO
    close(1)

    ! Read the edge list and find the maximum degree
    deg = 0
    open(unit = 1, file = 'edges.txt')
    DO i = 1, 10000000
        read(1, *, end = 10) u, v, dist
        deg(u) = deg(u) + 1
        deg(v) = deg(v) + 1
    ENDDO
10  continue
    close(1)

    ! Construct the adj list and delay matrix
    max_deg = maxval(deg)
    allocate(adj_list(num_vert, max_deg))
    deg = 0
    adj_list = 0
    delay = 0
    open(unit = 1, file = 'edges.txt')
    DO i = 1, 10000000
        read(1, *, end = 20) u, v, dist
        deg(u) = deg(u) + 1
        deg(v) = deg(v) + 1
        adj_list(u, deg(u)) = v
        adj_list(v, deg(v)) = u
        delay(u, v) = int(dist / (50.0 * 24.0 * dt), 8) ! Assuming the speed to be 50km/h
    ENDDO
20  continue
    close(1)
    max_delay = maxval(delay)

    ! Initialize the compartments
    ! Initially all are susceptibles
    allocate(x(n1*n2 * num_vert, tot_time + max_delay))
    allocate(age_groups_fracs(n2))
    allocate(age_rate_factor(n2))
    allocate(age_death_factor(n2))
    if (n2 .eq. 3) then
        !age distributeion https://en.wikipedia.org/wiki/Demographics_of_India
        age_groups_fracs(1) = 0.286  !0-14
        age_groups_fracs(2) =  .636  !15-64
        age_groups_fracs(3) = 1.-age_groups_fracs(1)-age_groups_fracs(2) ! 65+
        age_rate_factor(1)=1.  !
        age_rate_factor(2)=1.  ! recovery rates to be scaled such that average is 1.
        age_rate_factor(3)=1. !
        age_death_factor(3)=1.  !
        age_death_factor(2)=1.  ! recovery rates to be scaled such that average is 1.
        age_death_factor(1)=1.!
    else
        age_groups_fracs = 1./n2
        age_death_factor=1.
        age_rate_factor=1.
    endif
    x = 0    
    DO i = 1, n1*n2 * num_vert, n1*n2
        ix = (i-1)/(n1*n2) + 1
        Do j = 1, n2 ! Loop over age groups
            ! The population of each node is divided into different 
            ! susceptible groups
            x(i-1+j, :) = age_groups_fracs(j) * pop(ix)
        Enddo
    ENDDO

    ! Expose a few cities initially
    print*, "WARNING. All cities infected. init_condition.dat file ignored."
    tot_init_infected=num_vert

    allocate(init_cities(tot_init_infected), init_exposed_amount(tot_init_infected), &
        & init_sym_amount(tot_init_infected), init_asym_amount(tot_init_infected))
    !open(unit = 22, file = 'init_conditions.dat')
    !DO i = 1, tot_init_infected
    !    read(22, *)init_cities(i), init_exposed_amount(i), init_sym_amount(i), init_asym_amount(i)
    !ENDDO
    do i = 1, num_vert
        init_cities(i)=i
        init_exposed_amount(i)=1.e-3
        init_asym_amount=1.e-4
        init_sym_amount(i)=1.e-5
    enddo
    
    DO i = 1, size(init_cities)
        ix = init_cities(i)
        ! Increase exposed
        Do j = 1, n2
            x((ix-1)*n1*n2 + n2 + j, :) = init_exposed_amount(i)  * x((ix-1)*n1*n2 + j, :)
            x((ix-1)*n1*n2 + 2*n2 + j, :) = init_sym_amount(i)* x((ix-1)*n1*n2 + j, :)
            x((ix-1)*n1*n2 + 3*n2 + j, :) = init_asym_amount(i)* x((ix-1)*n1*n2 + j, :)
        Enddo
        ! Reduce susceptibles
        Do j = 1, n2
            x((ix-1)*n1*n2 + j, :) = (1.-init_exposed_amount(i)&
               &-init_sym_amount(i)-init_asym_amount(i)) * x((ix-1)*n1*n2 + j, :)
        Enddo
    ENDDO
    ! Read the balance parameters
    open(unit = 1, file = 'balance_params.dat')
    DO i = 1, num_vert
        read(1, *)eta(i)
    ENDDO
    close(1)
    
    ! Open output files
    open(unit = 101, file = 'Output_data/susceptibles_9d.dat')
    open(unit = 102, file = 'Output_data/exposed_9d.dat')
    open(unit = 103, file = 'Output_data/presympt_9d.dat')
    open(unit = 104, file = 'Output_data/asympt_9d.dat')
    open(unit = 105, file = 'Output_data/sympt_severe_9d.dat')
    open(unit = 106, file = 'Output_data/sympt_minor_9d.dat')
    open(unit = 107, file = 'Output_data/hospitalized_9d.dat')
    open(unit = 108, file = 'Output_data/recovered_9d.dat')
    open(unit = 109, file = 'Output_data/died_9d.dat')
    open(unit = 110, file = 'Output_data/peaks_9d.dat')
    if (n2 .eq. 3) then
        print*, "Need to writa ll"
        open(unit = 1010, file = 'Output_data/susceptibles_9d_1.dat')
        open(unit = 1011, file = 'Output_data/exposed_9d_1.dat')
        open(unit = 1012, file = 'Output_data/presympt_9d_1.dat')
        open(unit = 1013, file = 'Output_data/asympt_9d_1.dat')
        open(unit = 1014, file = 'Output_data/sympt_severe_9d_1.dat')
        open(unit = 1015, file = 'Output_data/sympt_minor_9d_1.dat')
        open(unit = 1016, file = 'Output_data/hospitalized_9d_1.dat')
        open(unit = 1017, file = 'Output_data/recovered_9d_1.dat')
        open(unit = 1018, file = 'Output_data/died_9d_1.dat')
        open(unit = 1020, file = 'Output_data/susceptibles_9d_2.dat')
        open(unit = 1021, file = 'Output_data/exposed_9d_2.dat')
        open(unit = 1022, file = 'Output_data/presympt_9d_2.dat')
        open(unit = 1023, file = 'Output_data/asympt_9d_2.dat')
        open(unit = 1024, file = 'Output_data/sympt_severe_9d_2.dat')
        open(unit = 1025, file = 'Output_data/sympt_minor_9d_2.dat')
        open(unit = 1026, file = 'Output_data/hospitalized_9d_2.dat')
        open(unit = 1027, file = 'Output_data/recovered_9d_2.dat')
        open(unit = 1028, file = 'Output_data/died_9d_2.dat')
        open(unit = 1030, file = 'Output_data/susceptibles_9d_3.dat')
        open(unit = 1031, file = 'Output_data/exposed_9d_3.dat')
        open(unit = 1032, file = 'Output_data/presympt_9d_3.dat')
        open(unit = 1033, file = 'Output_data/asympt_9d_3.dat')
        open(unit = 1034, file = 'Output_data/sympt_severe_9d_3.dat')
        open(unit = 1035, file = 'Output_data/sympt_minor_9d_3.dat')
        open(unit = 1036, file = 'Output_data/hospitalized_9d_3.dat')
        open(unit = 1037, file = 'Output_data/recovered_9d_3.dat')
        open(unit = 1038, file = 'Output_data/died_9d_3.dat')
    endif
    
    call RK4(x, n1, n2, num_vert, tot_time, max_deg, max_delay, pop, deg, eta, delay, D, dt, adj_list)    

End program many_comparts


  ! split a string into 2 either side of a delimiter token
  SUBROUTINE split_string(instring, string1, string2, delim)
    CHARACTER(30) :: instring,delim
    CHARACTER(30),INTENT(OUT):: string1,string2
    INTEGER :: index

    instring = TRIM(instring)

    index = SCAN(instring,delim)
    string1 = instring(1:index-1)
    string2 = instring(index+1:)

  END SUBROUTINE split_string


