Module equations
    implicit none

    ! Variable names and indices:
    ! 1 : susceptible (s), 2 : exposed (e), 3 : pre-symptomatic (ip), 4: asymptomatic (ia), 5 : symptomatic,severe (is)
    ! 6 : symptomatic,minor (im), 7 : hospitalized (h), 8 : recovered (r), 9 : dead (d)
    ! parameters names should be self-explanatory now
    real*8 :: se_orig, eip, eia, ipis, ipim, iar, ish, imr, hr, hd, lcdn_val,q_tau
    real*8 , allocatable :: age_rate_factor(:),age_death_factor(:)
    integer*8 :: begin_lcdn, end_lcdn

    !open(unit = 1, file = 'model_params.dat')
    !read(1, *)se, eip, eia, ipis, ipim, iar, ish, imr, hr, hd
    !close(1)

    !real*8 :: se = 1.5
    !real*8 :: eip = 0.1 
    !real*8 :: eia = 0.1
    !real*8 :: ipis = 0.4
    !real*8 :: ipim = 0.1
    !real*8 :: iar = 0.2
    !real*8 :: ish = 0.3
    !real*8 :: imr = 0.4
    !real*8 :: hr = 0.5
    !real*8 :: hd = 0.6
    
    contains
    Subroutine derivative(n1, n2, num_vert, t, dt, tot_time, max_deg, max_delay, se, deg, x, xprime, delay, pop, eta, D, adj_list)
        implicit none
        integer*8 :: i, j, k, ix, n1, n2, num_vert, t, tot_time, nbr, delta, max_deg, max_delay
        integer*8 :: deg(num_vert), delay(num_vert, num_vert), adj_list(num_vert, max_deg)
        real*8 :: eta_by_d, tot_infect, D,dt, se(num_vert)
        real*8 :: x(n1*n2*num_vert, tot_time + max_delay), xprime(n1*n2*num_vert), pop(num_vert), eta(num_vert), coupling(9*n2)
    
        !se = 1.5; eip = 0.1; eia = 0.1; ipis = 0.4; ipim = 0.1; iar = 0.2; ish = 0.3; imr = 0.4; hr = 0.5; hd = 0.6
    
        DO i = 1, n1*n2 * num_vert, n1*n2
            ! get vertex index
            ix = (i-1)/(n1*n2) + 1
            ! Calculate the couplings
            coupling = 0
            Do j = 1, deg(ix)
                nbr = adj_list(ix, j)
                eta_by_d = eta(nbr) / deg(nbr)
                delta = delay(ix,nbr)
                do k = 1, n2
                    coupling(k)      = coupling(k)      + eta_by_d * x((nbr-1)*n1*n2+k,t-delta)
                    coupling(k+n2)   = coupling(k+n2)   + eta_by_d * x((nbr-1)*n1*n2+n2+k,t-delta)
                    coupling(k+2*n2) = coupling(k+2*n2) + eta_by_d * x((nbr-1)*n1*n2+2*n2+k,t-delta)
                    coupling(k+3*n2) = coupling(k+3*n2) + eta_by_d * x((nbr-1)*n1*n2+3*n2+k,t-delta)
                    coupling(k+4*n2) = coupling(k+4*n2) + eta_by_d * x((nbr-1)*n1*n2+4*n2+k,t-delta)
                    coupling(k+5*n2) = coupling(k+5*n2) + eta_by_d * x((nbr-1)*n1*n2+5*n2+k,t-delta)
                    coupling(k+6*n2) = coupling(k+6*n2) + eta_by_d * x((nbr-1)*n1*n2+6*n2+k,t-delta)
                    coupling(k+7*n2) = coupling(k+7*n2) + eta_by_d * x((nbr-1)*n1*n2+7*n2+k,t-delta)
                    coupling(k+8*n2) = coupling(k+8*n2) + eta_by_d * x((nbr-1)*n1*n2+8*n2+k,t-delta)
                enddo
            Enddo
            ! sum all the infected population across all age groups and disease compartments
            tot_infect = 0.
            !q_tau=0.
            Do j = 1, n2 ! This loop is over age compartments
                tot_infect = tot_infect + x(i-1+2*n2+j,t) + (2./3.) * x(i-1+3*n2+j,t) + x(i-1+4*n2+j,t) + x(i-1+5*n2+j,t)
                !if ((t .ge. begin_lcdn/dt ) .and. (t .le. end_lcdn/dt)) then
                !        tot_infect = exp(-0.001*(t-max_delay))*tot_infect
                !elseif  (t .gt. end_lcdn/dt) then
                !        tot_infect = exp(-0.001*end_lcdn/dt)*tot_infect
                !endif
            Enddo
            tot_infect = exp(-q_tau*(t-max_delay)*dt)*tot_infect
            Do j = 1, n2 ! This loop is over age compartments
                xprime(i-1+j)   = - (se(ix)/pop(ix)) * x(i-1+j,t) * tot_infect  & 
                    & - D * (eta(ix) * x(i-1+j,t) - coupling(j))  
                xprime(i-1+n2+j) = (se(ix)/pop(ix)) * x(i-1+j,t) * tot_infect &
                    &- eip *age_rate_factor(j)* x(i-1+n2+j,t) - eia *age_rate_factor(j)* x(i-1+n2+j,t) &
                    & - D * (eta(ix) * x(i-1+n2+j,t) - coupling(j+n2))
                xprime(i-1+2*n2+j) = eip *age_rate_factor(j)* x(i-1+n2+j,t)&
                    & - ipis *age_rate_factor(j)* x(i-1+2*n2+j,t) - ipim *age_rate_factor(j)* x(i-1+2*n2+j,t) &
                    & - D * (eta(ix) * x(i-1+2*n2+j,t) - coupling(j+2*n2))
                xprime(i-1+3*n2+j) = eia *age_rate_factor(j)* x(i-1+n2+j, t)&
                    & - iar *age_rate_factor(j)* x(i-1+3*n2+j,t) &
                    & - D * (eta(ix) * x(i-1+3*n2+j,t) - coupling(j+3*n2))
                xprime(i-1+4*n2+j) = ipis *age_rate_factor(j)* x(i-1+2*n2+j,t)&
                    & - ish *age_rate_factor(j)* x(i-1+4*n2+j,t) & 
                    & - D * (eta(ix) * x(i-1+4*n2+j,t) - coupling(j+4*n2))
                xprime(i-1+5*n2+j) = ipim *age_rate_factor(j)* x(i-1+2*n2+j,t) &
                    & - imr *age_rate_factor(j)* x(i-1+5*n2+j,t) &
                    & - D * (eta(ix) * x(i-1+5*n2+j,t) - coupling(j+5*n2))
                xprime(i-1+6*n2+j) = ish *age_rate_factor(j)* x(i-1+4*n2+j,t) &
                    & - hr *age_rate_factor(j)* x(i-1+6*n2+j,t) &
                    & - hd *age_death_factor(j)* x(i-1+6*n2+j,t) &
                    & - D * (eta(ix) * x(i-1+6*n2+j,t) - coupling(j+6*n2))
                xprime(i-1+7*n2+j) = iar *age_rate_factor(j)* x(i-1+3*n2+j,t)&
                    & + imr *age_rate_factor(j)* x(i-1+5*n2+j,t) + hr *age_rate_factor(j)* x(i-1+6*n2+j,t) &
                    & - D * (eta(ix) * x(i-1+7*n2+j,t) - coupling(j+7*n2))
                xprime(i-1+8*n2+j) = hd *age_death_factor(j)* x(i-1+6*n2+j,t) 
            Enddo
        ENDDO
    End subroutine derivative
End module equations
