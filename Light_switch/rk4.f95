Module rk4_integrate
    use equations
    implicit none
    contains
    Subroutine RK4(x, n1, n2, num_vert, tot_time, max_deg, max_delay, pop, deg, eta, delay, D, dt, adj_list)    
        implicit none
        integer*8 :: i, j, l, ix, t, n1, n2, num_vert, tot_time, max_deg, max_delay, lcdn_on(num_vert), s1, s2
        integer*8 :: deg(num_vert), delay(num_vert, num_vert), adj_list(num_vert, max_deg)
        real*8 :: dt, D, se(num_vert), peak_time(n1*n2*num_vert), lcdn_duration(num_vert)
        real*8 :: eta(num_vert), pop(num_vert), x(n1*n2*num_vert, tot_time + max_delay)
        real*8 :: xprime(n1*n2*num_vert), k(n1*n2*num_vert, 4), peak(n1*n2*num_vert)
    
        ! Integration starts here
        lcdn_on = 0
        lcdn_duration = 0
        se = se_orig
        peak = x(:, max_delay+1)
        peak_time = 1
        DO t = max_delay + 1, tot_time + max_delay - 1
            !if ((t .ge. begin_lcdn/dt ) .and. (t .le. end_lcdn/dt)) then
            !        se=se*lcdn_val
            !endif
            !write(*,'(I10,A)',ADVANCE='NO') t, CHAR(13)

            !Start a lockdown if the number of cases goes above 500
            Do ix = 1, num_vert
                if (lcdn_on(ix) == 0) then
                    s1 = (ix-1)*n1*n2 + 6*n2 + 1
                    s2 = (ix-1)*n1*n2 + 6*n2 + n2
                    if (sum(x(s1:s2, t)) > 500./25000) then
                        se(ix) = se_orig * lcdn_val
                        print*, se(ix), se_orig
                        lcdn_on(ix) = 1
                        lcdn_duration(ix) = 0
                    endif
                else
                    if (lcdn_duration(ix) < 14)then
                        lcdn_duration(ix) = lcdn_duration(ix) + dt
                    else
                        se(ix) = se_orig
                        lcdn_on(ix) = 0
                        lcdn_duration(ix) = 0
                    endif
                endif
            Enddo
            call derivative(n1, n2, num_vert, t, dt, tot_time, max_deg, max_delay, se, deg, x, xprime, delay, pop, eta, D, adj_list)
            k(:, 1) = xprime(:)
    
            x(:, t+1) = x(:, t) + 0.5 * k(:, 1) * dt
            call derivative(n1, n2, num_vert, t, dt, tot_time, max_deg, max_delay, se, deg, x, xprime, delay, pop, eta, D, adj_list)
            k(:, 2) = xprime(:) 
    
            x(:, t+1) = x(:, t) + 0.5 * k(:, 2) * dt
            call derivative(n1, n2, num_vert, t, dt, tot_time, max_deg, max_delay, se, deg, x, xprime, delay, pop, eta, D, adj_list)
            k(:, 3) = xprime(:)
    
            x(:, t+1) = x(:, t) + k(:, 3) * dt
            call derivative(n1, n2, num_vert, t, dt, tot_time, max_deg, max_delay, se, deg, x, xprime, delay, pop, eta, D, adj_list)
            k(:, 4) = xprime(:)
    
            x(:, t+1) = x(:, t) + (1/6.) * (k(:, 1) + 2. * k(:, 2) + 2. * k(:, 3) + k(:, 4)) * dt

            ! Detect the peak value and the time
            Do i = 1, n1*n2*num_vert
                if (x(i, t+1) > peak(i))then
                    peak(i) = x(i, t+1)
                    peak_time(i) = (t - max_delay)*dt
                endif
            Enddo

            ! If any compartment value corresponds to less than 1 person, make it zero
            !where (x(1, t+1) < )
            if (x(1, t+1) <= 1./25000.) x(1, t+1) = 0.d0
            if (x(2, t+1) <= 1./25000.) x(2, t+1) = 0.d0

            if (all(x(3:6, t+1) <= 1./25000))then
               x(3:6, t+1) = 0.d0
            endif

            if (x(7, t+1) <= 1./25000.) x(7, t+1) = 0.d0
            if (x(8, t+1) <= 1./25000.) x(8, t+1) = 0.d0
            if (x(9, t+1) <= 1./25000.) x(9, t+1) = 0.d0
    
            ! For now, writing only susceptibles and exposed
            if (mod(t, 10) == 0)then

                write(101, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2 + 1        : (i-1)*n1*n2 + n2, t)),   i = 1, num_vert)
                write(102, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2 + n2   + 1 : (i-1)*n1*n2 + 2*n2, t)), i = 1, num_vert)
                write(103, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2 + 2*n2 + 1 : (i-1)*n1*n2 + 3*n2, t)),  i = 1, num_vert)
                write(104, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2 + 3*n2 + 1 : (i-1)*n1*n2 + 4*n2, t)), i = 1, num_vert)
                write(105, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2 + 4*n2 + 1 : (i-1)*n1*n2 + 5*n2, t)), i = 1, num_vert)
                write(106, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2 + 5*n2 + 1 : (i-1)*n1*n2 + 6*n2, t)), i = 1, num_vert)
                write(107, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2 + 6*n2 + 1 : (i-1)*n1*n2 + 7*n2, t)), i = 1, num_vert)
                write(108, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2 + 7*n2 + 1 : (i-1)*n1*n2 + 8*n2, t)), i = 1, num_vert)
                write(109, *)(t-max_delay)*dt, &
                        &(sum(x((i-1)*n1*n2 + 8*n2 + 1 : (i-1)*n1*n2 + 9*n2, t)), i = 1, num_vert)

                if (n2 == 3 ) then ! write age separated files
                  do l = 0,n1-1 
                    do j=1,n2
                        write(1000+(10*j)+l,*) (t-max_delay)*dt,  (x((i-1)*n1*n2 + l*n2+ j,t), i=1,num_vert)
                     enddo
                  enddo
                endif
            endif

        ENDDO
        print*, " "
        Do i = 1, n1*n2*num_vert
            write(110, *)peak(i), peak_time(i)
        Enddo
    End Subroutine RK4

End module rk4_integrate
