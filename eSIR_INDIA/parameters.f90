! Developed by Dhiraj Kumar Hazra (dhirajhazra@gmail.com)
! Parameters that are used globally
module parameters
    USE RKSUITE_90_PREC
    implicit none
    Integer::states,people_categories,Lightswitch_duration
    Integer,parameter::num_base_eq=9,total_state_num=36
    integer::steps_days=200,Cycle_start_day,Cycle_duration,Cycle_break     
    INTEGER,PARAMETER::susceptible=1,exposed=2,asymptomatic=3
    INTEGER,PARAMETER::symptomatic=4,mild=5,severe=6
    INTEGER,PARAMETER::hospitalized=7,dead=8,recovered=9
    REAL(WP)::Lightswitch_Threshold,Lightswitch_off_Tol,Hospital_Threshold
    REAL(WP),allocatable,dimension(:)::Initial_E,Initial_I_ASY,Initial_I_SY
    REAL(WP)::DAY_ONE=1d0,DAY_FINAL=100d0 ! Start and final dates
    REAL(WP)::Initial_eff_asymp=0.5,Initial_eff_symp=1d0,Initial_eff_mild=0.8,Initial_eff_svr=1d0    
    REAL(WP)::eff_asymp,eff_symp,eff_mild,eff_svr,decay_time_asymp
    REAL(WP)::decay_time_svr,decay_time_symp,decay_time_mild
    REAL(WP):: beta_param=3d-1,mobility_param=0d0,E_ASY=4d-2,E_SY=5d-2,SY_M=4d-1
    REAL(WP):: ASY_R=1d-1,SY_SV=2d-1,M_R=2d-1,SV_H=1d-1,H_D=4d-2,H_R=6d-2
    REAL(WP)::eff_beta_block=1d0,eff_mob_block=1d0,beta_block,mob_block
    real(WP),dimension(:,:,:),allocatable::initial_conditions
    real(WP),dimension(:,:),allocatable::Entire_Migration_matrix
    real(WP),dimension(:,:),allocatable::population_fraction,allstate_population_fraction
    real(WP),dimension(:,:),allocatable::S_INI,E_INI,ISY_INI,IASY_INI    
    real(WP),dimension(:,:),allocatable::IM_INI,ISV_INI,IH_INI,D_INI,R_INI
    real(WP),dimension(:,:),allocatable::C_E2IASY,C_E2ISY,C_IASY2R
    real(WP),dimension(:,:),allocatable::C_ISY2MLD,C_ISY2SVR,C_MLD2R
    real(WP),dimension(:,:),allocatable::C_SVR2HPTL,C_HPTL2D,C_HPTL2R
    real(WP),dimension(:,:,:),allocatable::beta_matrix,mobility_matrix
    real(WP),dimension(:),allocatable::fraction_AS,fraction_MILD,fraction_dead    
    integer,dimension(:),allocatable::selected_population,Selected_states_indices
    character(LEN=64),allocatable,dimension(:)::selected_states,category_name
    character(LEN=128),allocatable,dimension(:,:)::write_filename
    character(LEN=64),dimension(total_state_num)::state_names    
    logical::Implement_lockdown=.False.,lightswitch=.False.,Hospital_Controlled=.False.
    logical::cyclic_lockdown=.False.,Mobility=.False.,Use_Uniform_Init=.True.
    logical,allocatable,dimension(:,:)::lockdown
end module parameters

        
