! Developed by Dhiraj Kumar Hazra (dhirajhazra@gmail.com)
! This is the main driver.
program esir
    USE RKSUITE_90_PREC
    use parameters
    use system_of_equations
    use utils
    use engine
    USE INIFILE     
    implicit none
    Real(WP),dimension(:),allocatable::save_day    
    Real(WP),dimension(:,:,:,:),allocatable::SEIR_STORE_ARRAY    
    Real(WP),dimension(total_state_num)::population    
    real(wp)::migration_in_lakhs,test_beta,R_0
    integer::i,j,k,filenum_data,this_state,this_state_num(1),filenum_result,Num_M_data
    integer::idx(1),jdx(1),pop1,pop2,Migration_duration,Num_corr,test_deltat
    integer::Lockdown_start_day,Lockdown_duration,Num_Lockdown,this_day,This_Cycle_start_day
    character(LEN=64)::datafile,status,foldername,key_frac,this_category
    character(LEN=64)::this_lockdown,state1,state2,category_num,key_name,key_mild,key_dead 
    character(LEN=Ini_max_string_len)::InputFile,key_lock_day,key_lock_duration 
    logical::bad,Implement_Rapid_Quarantine_Effect,Write_Header,First_Beta,Uniform_fraction,Uni_Pop_Frac
    
    !INIFILE Initialization
    !=================== Input file   =========================
    InputFile = ''
  
    if (iargc() /= 0)  call getarg(1,InputFile)
    if (InputFile == '') stop 'No parameter input file'

    call Ini_Open(InputFile, 1, bad, .false.)
    if (bad) stop 'Error opening parameter file'
    Ini_fail_on_not_found = .false.
    
      !========  Reading from file ===============================
    datafile=Ini_Read_string('State_filename')
    
    
    ! Read data (state names and population)
    ! Read only states and not UT for now. 
    ! loop over 29 states and 7 UTs    
    Print*,'Reading data from:',datafile
    
    OPEN(NEWUNIT=filenum_data,FILE=datafile,action='read',status='old')  
    do this_state=1,total_state_num 
    read(filenum_data,*)status,state_names(this_state),population(this_state)    
    end do
    close(filenum_data)
    !========================================================================
    !========================================================================
    allocate(Entire_Migration_matrix(total_state_num,total_state_num))
    Entire_Migration_matrix=0d0
    Mobility=Ini_Read_Logical('Allow_Mobility',.False.)    
    if(Mobility) then 
    datafile=Ini_Read_string('Migration_filename')
    Num_corr=Ini_Read_Int('Num_Migration_data') 
    OPEN(NEWUNIT=filenum_data,FILE=datafile,action='read',status='old')  
    do i=1,Num_corr 
    read(filenum_data,*)state1,state2,migration_in_lakhs    
        if(.not.(any(state_names==state1))) then 
            print*,trim(state1),' Not listed in State Database'
            Stop
        else if(.not.(any(state_names/=state2))) then 
            print*,trim(state2),' Not listed in State Database'
            Stop
        end if
        
       idx(1)=findloc(state_names,state1,dim=1) 
       pop1=population(idx(1)) 
       jdx(1)=findloc(state_names,state2,dim=1) 
       pop2=population(jdx(1)) 
       Entire_Migration_matrix(idx(1),jdx(1))=migration_in_lakhs*100000d0/pop2
       print*,'Indices of the states',idx(1),jdx(1)
    end do
    close(filenum_data)

    Migration_duration=Ini_Read_Int('MOBILITY_DAYS')
    Entire_Migration_matrix=Entire_Migration_matrix/dble(Migration_duration)
    Print*,'Migration Matrix Reading Done: Mobility Matrix will populate from here'
    else
    Entire_Migration_matrix=0d0
    end if
    !========================================================================
    !========================================================================
    
    ! Now select the number of states we plan to work with
    states=Ini_Read_Int('Number_of_States')    
    people_categories=Ini_Read_Int('Number_of_Categories') ! Division can be age-group, socio-economic etc.
    !========================================================================
    !========================================================================
    !========================================================================
    !========================================================================
    
    Print*,'Initializing'

    call initialize(states,people_categories)

    if(people_categories==1) category_name(1)='Unified' ! Single category considered
    ! population_fraction must be initiated
    Uni_Pop_Frac=Ini_Read_Logical('Uniform_Population_Fraction',.False.)    
    if(people_categories>1) then
        do i=1,people_categories
            write(this_category,'(i10)') i
            if(Uni_Pop_Frac) then
            if(i<people_categories) then
                key_frac=trim('Population_fraction_')//trim(adjustl(this_category))        
                population_fraction(i,:)=Ini_Read_double(key_frac)
            end if
            end if
            key_name=trim('Name_Category_')//trim(adjustl(this_category))
            category_name(i)=Ini_Read_string(key_name)
        end do
    end if

    if(states==total_state_num) then ! Just do all of them. No need to select
    selected_states=state_names
    selected_population=population    
    else
    !=========================================================================
    ! Provide state names
    ! in the file
    Print*,'Reading selected states only'
    datafile=Ini_Read_string('Select_States_Filename')
    
    OPEN(NEWUNIT=filenum_data,FILE=datafile,action='read',status='old')  
    do this_state=1,states 
    read(filenum_data,*)selected_states(this_state)    
    end do
    close(filenum_data)
        
    !===========================================================================
    ! Identify the state and get the population
    do i=1,states
       if(any(state_names==selected_states(i))) then 
       this_state_num(1)=findloc(state_names,selected_states(i),dim=1) 
       selected_population(i)=population(this_state_num(1)) 
       selected_states_indices(i)=this_state_num(1)
       print*,'Selected states are: ',trim(adjustl(selected_states(i))),' with population',selected_population(i)
       else
       print*,selected_states(i)
       stop 'Wrong State Name'
       end if
    end do
    end if    

    if(.not.(Uni_Pop_Frac)) then     
        print*,'Reading population fraction'
        datafile=Ini_Read_string('Population_Fraction_Filename')    
        OPEN(NEWUNIT=filenum_data,FILE=datafile,action='read',status='old') 
            do this_state=1,total_state_num 
                read(filenum_data,*)state1,allstate_population_fraction(:,this_state)
            end do                
        close(filenum_data)
        if(states==total_state_num) then      
            population_fraction=allstate_population_fraction       
        else       
            do this_state=1,states
                population_fraction(:,this_state)=allstate_population_fraction(:,selected_states_indices(this_state))
            end do        
        end if
        
    end if
         
!     print*,selected_states_indices     
!     print*,population_fraction
!     pause
    
    ! Read the final day for simulation
    DAY_FINAL=Ini_Read_double('End_date')    
    steps_days=int(day_final) ! Stepsize of 1 day in saving the data
    
    Use_Uniform_Init=Ini_Read_Logical('Impose_Uniform_Init',.True.)        
    if(Use_Uniform_Init) then 
        Initial_E(:)=Ini_Read_double('INITIAL_EXPOSED')
        Initial_I_SY(:)=Ini_Read_double('INITIAL_INFECTED_SYMPTOMATIC')
        Initial_I_ASY(:)=Ini_Read_double('INITIAL_INFECTED_ASYMPTOMATIC')
        
    else    
        if(states==total_state_num) then 
            datafile=Ini_Read_string('State_Initial_Condition_Filename')
        else  
            datafile=Ini_Read_string('Select_States_Filename')
        end if
        OPEN(NEWUNIT=filenum_data,FILE=datafile,action='read',status='old')  
        do i=1,states
        read(filenum_data,*)state1,Initial_E(i),Initial_I_ASY(i),Initial_I_SY(i)
        end do
        close(filenum_data)
    end if
!     print*,Initial_I_SY,Initial_I_ASY
    
    beta_param=Ini_Read_double('BETA_PARAM')
    E_ASY=Ini_Read_double('Exposed_to_Asymptomatic')
    E_SY=Ini_Read_double('Exposed_to_Symptomatic')
    ASY_R=Ini_Read_double('Asymptomatic_to_Recovered')
    SY_M=Ini_Read_double('Symptomatic_to_Mild')
    M_R=Ini_Read_double('Mild_to_Recovered')    
    SY_SV=Ini_Read_double('Symptomatic_to_Severe')
    SV_H=Ini_Read_double('Severe_to_Hospitalized')    
    H_D=Ini_Read_double('Hospitalized_to_Dead')    
    H_R=Ini_Read_double('Hospitalized_to_Recovered')    

    Initial_eff_asymp=Ini_Read_double('Efficiency_asymptomatic')    
    Initial_eff_symp=Ini_Read_double('Efficiency_symptomatic')    
    Initial_eff_mild=Ini_Read_double('Efficiency_mild')    
    Initial_eff_svr=Ini_Read_double('Efficiency_severe')    

    
    Implement_Rapid_Quarantine_Effect=Ini_Read_Logical('Implement_Rapid_Test_N_Quarantine_Effect',.False.) 
    if(Implement_Rapid_Quarantine_Effect) then 
    decay_time_asymp=Ini_Read_double('Test_improvement_Timescale_ASYMP')    
    decay_time_symp=Ini_Read_double('Test_improvement_Timescale_SYMP')    
    decay_time_mild=Ini_Read_double('Test_improvement_Timescale_Mild')    
    decay_time_svr=Ini_Read_double('Test_improvement_Timescale_SVR')    
    
    else 
    decay_time_asymp=1d30
    decay_time_symp=1d30
    decay_time_mild=1d30
    decay_time_svr=1d30
    end if

    ! We assume Asymptomatic fraction is age independent
    Uniform_fraction=Ini_Read_Logical('Uniform_Fraction',.False.)    
    fraction_as=Ini_Read_double('Asymptomatic_fraction')    

    
    if(Uniform_fraction) then 
        fraction_mild(:)=Ini_Read_double('Mild_fraction')    
        fraction_dead(:)=Ini_Read_double('Death_fraction')    
    else
        do i=1,people_categories
            write(this_category,'(i10)') i
            key_mild=trim('Mild_fraction_Age_Group_')//trim(adjustl(this_category))        
            key_dead=trim('Death_fraction_Age_Group_')//trim(adjustl(this_category))
            
            fraction_mild(i)=Ini_Read_double(key_mild)    
            fraction_dead(i)=Ini_Read_double(key_dead)    
        end do
        
    end if
    
    

    ! Intervention
!     pause 'Implement zero infectivity when all channels apart from H,R and D are less than 1'
    Implement_lockdown=Ini_Read_Logical('Implement_Lockdown',.False.)    
    LightSwitch=Ini_Read_Logical('Lightswitch_Lockdown',.False.)
    cyclic_lockdown=Ini_Read_Logical('Cyclic_Lockdown',.False.)
    
    if(LightSwitch.and.cyclic_lockdown) stop 'Two lockdown strategies are not implemented'
    
    if(Implement_lockdown) then 
        allocate(lockdown(states,int(DAY_FINAL)))
        lockdown=.False.
        
        if(LightSwitch) then 

        Lightswitch_Threshold=Ini_Read_Double('Lightswitch_Threshold')
        Lightswitch_duration=Ini_Read_Int('Lightswitch_Lockdown_Duration')
        Hospital_Controlled=Ini_Read_Logical('Lightswitch_Hospital_Control')
        if(Hospital_Controlled) then 
         Hospital_Threshold=Lightswitch_Threshold
         Lightswitch_off_Tol=Ini_Read_Double('Lightswitch_Off_Tolerence')
        end if
        
        elseif(cyclic_lockdown) then 
        
        Cycle_start_day=Ini_Read_Int('Lockdown_Cycle_Start')    
        Cycle_duration=Ini_Read_Int('Lockdown_Cycle_Duration')    
        Cycle_break=Ini_Read_Int('Lockdown_Cycle_Break')
        
        this_day=1
        This_Cycle_start_day=Cycle_start_day
        do while(this_day<int(DAY_FINAL))
            this_day=This_Cycle_start_day
            lockdown(1:states,this_day:min(this_day+Cycle_duration-1,int(DAY_FINAL)))=.True.            
            This_Cycle_start_day=this_day+Cycle_duration+Cycle_break
        end do         
!         do i=1,steps_days 
!             print*,i,lockdown(states,i)
! !             pause
!         end do
!         pause
        else 
        Num_Lockdown=Ini_Read_Int('Number_of_Lockdowns')

        do i=1,Num_Lockdown
            write(this_lockdown,'(i10)') i
            
            key_lock_day=trim('Day_of_Lockdown_')//trim(adjustl(this_lockdown))
            key_lock_duration=trim('Duration_of_Lockdown_')//trim(adjustl(this_lockdown))
            Lockdown_start_day=Ini_Read_Int(key_lock_day)    
            Lockdown_duration=Ini_Read_Int(key_lock_duration)    
            lockdown(1:states,Lockdown_start_day:Lockdown_start_day+Lockdown_duration-1)=.True.            
        end do
        end if

        beta_block=Ini_Read_double('Beta_Block_Lockdown')    
        mob_block=Ini_Read_double('Mobility_Block_Lockdown')    
    
    end if
    
   
     First_Beta=.True.        
      do test_beta=0.8,0.14,-0.003
       beta_param=test_beta
       if(LightSwitch) Lockdown(:,:)=.False. ! For colorplot, this is important
       ! As the LightSwitch is controlled by the ODE itself so values are overwritten
!     print*,fraction_as,beta_param,beta_block,Initial_eff_asymp
!         pause
       Print*,'Getting Initial Conditions'
       call Calc_initial_conditions(initial_conditions,selected_population)         

    if(.not.(allocated(save_day))) allocate(save_day(steps_days))
    if(.not.(allocated(SEIR_STORE_ARRAY))) allocate(SEIR_STORE_ARRAY(steps_days,num_base_eq,people_categories,states))
    

    forall(i=1:steps_days) save_day(i)=day_one+((DBLE(I)-1.0_WP)*(day_final-day_one)/(DBLE(steps_days)-1.0_WP))    

    Print*,'Integrating'
!     print*,save_day
    call integrate(initial_conditions,SEIR_STORE_ARRAY,save_day)    
    
   ! Writing the results in plot_data folder 
    foldername='plot_data'
    do j=1,people_categories
        write(category_num,'(i10)') j
            R_0=fraction_as(j)*beta_param*Initial_eff_asymp&
             &*(1d0/ASY_R)+(1d0-fraction_as(j))*beta_param*Initial_eff_symp&
             &*((1d0/SY_M)+(fraction_mild(j)/M_R)+((1d0-fraction_mild(j))/SV_H))
             print*,'R_0',R_0
        
        do k=1,states
        
            if(First_Beta) Write_Header=.True.        
            write_filename(j,k)=trim(adjustl(foldername))//"/"//trim("Beta_Variation_SEIR_OUT_")//&
            &trim(adjustl(selected_states(k)))//trim("_people_category_")//trim(adjustl(category_num))//".txt"
            OPEN(NEWUNIT=filenum_result,FILE=write_filename(j,k),action='write',status = 'unknown', access = 'append')   
             if(Write_Header) write(filenum_result,*) 'Day beta R_0 Susceptible Exposed &
             &Asymptomatic Symptomatic Mild Severe Hospitalized Dead Recovered'
            Write_Header=.False.             
            do I=1,steps_days
            write(filenum_result,*)int(save_day(I)),beta_param,R_0,SEIR_STORE_ARRAY(I,:,J,K)
            end do
            write(filenum_result,*) ' '            
            Close(filenum_result)
        end do
    end do  
    First_Beta=.False.    
    end do
    
    Print*,'Data files written'
    
  ! This creat plot script  
!   call makeplots(write_filename,selected_states,category_name)
end program esir
