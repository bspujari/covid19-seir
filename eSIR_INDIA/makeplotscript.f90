! Developed by Dhiraj Kumar Hazra (dhirajhazra@gmail.com)
module utils
implicit none
contains
subroutine makeplots(filestoplot,statenames,categories)
    implicit none
    character(len=*),dimension(:,:),intent(in)::filestoplot
    character(len=*),dimension(:),intent(in)::statenames,categories
    character(len=64)::plotscript_file
    integer::GNUPLOT_UNIT,i,j
    
    plotscript_file='plots.p'
    OPEN(NEWUNIT=GNUPLOT_UNIT,FILE=plotscript_file,FORM='FORMATTED',STATUS='REPLACE')
    write(GNUPLOT_UNIT,*)'set te pdf color'
    write(GNUPLOT_UNIT,*)'set ytics format "%g"'
    write(GNUPLOT_UNIT,*)'set xtics format "%g"'
    write(GNUPLOT_UNIT,*)'set grid'
    write(GNUPLOT_UNIT,*)'set output "eSIR_state.pdf" '    
    write(GNUPLOT_UNIT,*)'set xlabel "t in days" font "Times-Roman,12" '
    write(GNUPLOT_UNIT,*)'set ylabel "Population in different conditions (t)" font "Times-Roman,12"'
    write(GNUPLOT_UNIT,*)'set xtics nomirror font "Times-Roman,12" '
    write(GNUPLOT_UNIT,*)'set ytics nomirror font "Times-Roman,12"'    
    write(GNUPLOT_UNIT,*)'set xrange [1:]'
    write(GNUPLOT_UNIT,*)'set yrange [1:]'    
    write(GNUPLOT_UNIT,*)'unset key'
    write(GNUPLOT_UNIT,*)'set title font "Times-Roman,12"'
    write(GNUPLOT_UNIT,*)'set key top right Right horizontal autotitle columnheader samplen -1 tc variable spacing 1'

    do i=1,size(filestoplot,1)
        do j=1,size(filestoplot,2)          
         write(GNUPLOT_UNIT,*)'unset logscale'
        
        write(GNUPLOT_UNIT,*)'set title "State: ',trim(adjustl(statenames(j)))&
            &,', population category: ',trim(adjustl(categories(i))),'"'
        write(GNUPLOT_UNIT,*)'plot ',' "',trim(filestoplot(i,j)),'" ','w l lw 3,',&
        &' ""  u 1:3 w l lw 3,',' "" u 1:4 w l lw 3,',' "" u 1:5 w l lw 3,',&
        &' ""  u 1:6 w l lw 3,',' "" u 1:7 w l lw 3,',' "" u 1:8 w l lw 3,',&
        &' ""  u 1:9 w l lw 3,',' "" u 1:10 w l lc rgb "grey" lw 3'        
        write(GNUPLOT_UNIT,*)'plot ',' "',trim(filestoplot(i,j)),'" ',' u 1:3 w l lw 3,'&
        &,' "" u 1:4 w l lw 3,',' "" u 1:5 w l lw 3,',&
        &' ""  u 1:6 w l lw 3,',' "" u 1:7 w l lw 3,',' "" u 1:8 w l lw 3,',&
        &' ""  u 1:9 w l lw 3'     
        write(GNUPLOT_UNIT,*)'set log y'
    
        write(GNUPLOT_UNIT,*)'plot ',' "',trim(filestoplot(i,j)),'" ',' u 1:3 w l lw 3,'&
        &,' "" u 1:4 w l lw 3,',' "" u 1:5 w l lw 3,',&
        &' ""  u 1:6 w l lw 3,',' "" u 1:7 w l lw 3,',' "" u 1:8 w l lw 3,',&
        &' ""  u 1:9 w l lw 3'        
    
        end do
    end do    
    close(GNUPLOT_UNIT)

    write(*,*) "PlotScript Created"        
    write(*,*) "Now run gnuplot > load 'plots.p'"        
    
end subroutine makeplots
end module utils
